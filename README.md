# Import / Export

Import/Export pre Nette Framework a CMS system WAME admin 3.x

Momentálne su 2 nezjednotene sposoby importu - pre DB a pre XML 

[TOC]

## Import

Konfiguracia je definovana v neon suboroch

`App\Model\ImportExport\Import`

### Metody

| metoda                | parametre                                 | povinne   | popis                                     |
|-----------------------|-------------------------------------------|-----------|-------------------------------------------|
| chunk                 | chunk:int                                 | nie       | Pocet poloziek v jednej davke importu     |


#### Databaza

| metoda                | parametre                                 | povinne   | popis                                     |
|-----------------------|-------------------------------------------|-----------|-------------------------------------------|
| call                  | function:callable                         | ano       | Zavola funkciu, ktora pracuje s importom  |
| convert               | converter:array/callable                  | nie       | Vykona zmena na hodnotach vystupu         |
| map                   | array:array                               | ano       | Namapuje kluce vystupu                    |
| select                | function:callable                         | nie       | Stlpce, ktore ma vytiahnut                |
| setInputRepository    | table:string, db:Nette\Database\Context   | ano       | Nastavi vstupny repozitar                 |
| setOutputRepository   | repository:BaseRepository                 | ano       | Nastavi vystupny repozitar                |

#### XML

| metoda                | parametre                                 | povinne   | popis                                     |
|-----------------------|-------------------------------------------|-----------|-------------------------------------------|
| convertMulti          | converter:array/callable                  | nie       | Vykona zmena na hodnotach vystupu         |
| mapMulti              | array:array                               | ano       | Namapuje kluce vystupu                    |
| setInputXml           | source:string                             | ano       | Cesta k XML suboru                        |
| setRepository         | array:array                               | ano       | Spristupni repozitar pod klucom           |


### Ulohy (calls)

#### Databaza

| nazov                 | parametre                                                             | popis                                     |
|-----------------------|-----------------------------------------------------------------------|-------------------------------------------|
| SaveToDatabase        | -                                                                     | Vlozi alebo aktualizuje udaje v databaze  |
| UpdateId              | column:string                                                         | Aktualizuje identifikator                 |
| UpdateParent          | column:string, importId:string, importParentId:string                 | Aktualizuje rodica                        |
| UpdateColumn          | columnI:string, columnO:string, columnIdI:string, columnIdO:string    | Aktualizuje rodica                        |

#### XML

| nazov                     | parametre                                                             | popis                                     |
|---------------------------|-----------------------------------------------------------------------|-------------------------------------------|
| InsertMultipleToDatabase  | outputKey:string, table:string, unique:bool, importColumn:string      | Vlozi alebo aktualizuje udaje v databaze  |


### Priklady

#### Databaza

```
MoneyProductImport:
    class: App\Model\ImportExport\Import
    setup:
        - chunk(1000)
        - setInputRepository(CSW_EObchod_Artikl, @database.money.context)
        - setOutputRepository(App\Model\Shop\ShopProductRepository())
        - map({
            ID: import_product_id
            Nazev: title
            Kod: plu
            Popis2: description
            Popis1: text
            Hmotnost: weight
            Delka: length
            Sirka: width
            Vyska: height
            Create_Date: create_date
            NadrazenyArtikl_ID: import_parent_id
        })
        - convert({
            status: 1
            lang: sk
        })
        - convert(App\Model\ImportExport\Converters\AliasFromTitle())
        - call(App\Model\ImportExport\Calls\SaveToDatabase())
        - call(App\Model\ImportExport\Calls\UpdateId(product_id))
        - call(App\Model\ImportExport\Calls\UpdateParent(descendant_product, import_product_id))
```

#### XML

```
services:
    MatterhornProductImport:
        class: Wame\ImportExport\Import
        setup:
            - chunk(10)
            - setInputXml('http://matterhorn-moda.sk/xmldata/hurtowniaen_sk_products.xml')
            # repositories
            - setRepository(wame_shop_settings, @App\Model\Shop\ShopSettingsRepository)
            - setRepository(wame_site_settings, @App\Model\System\SiteSettingsRepository)
            - setRepository(wame_shop_product, @App\Model\Shop\ShopProductRepository)
            - setRepository(wame_shop_category, @App\Model\Shop\ShopCategoryRepository)
            - setRepository(wame_shop_product_in_category, @App\Model\Shop\ShopProductInCategoryRepository)
            # map
            - mapMulti(product, {
                _id: [wame_shop_product, import_product_id]
                name: [wame_shop_product, title]
                category_path: [wame_shop_category, alias]
                category: [wame_shop_category, title]
                description: [wame_shop_product, description]
            }, {
                wame_shop_product: {
                    lang: sk
                    import_type: matterhorn
                    status: 1
                    user_id: 1
                }
                wame_shop_category: {
                    lang: sk
                    import_type: matterhorn
                    status: 1
                }
            })
            - convertMulti(wame_shop_product, Wame\ImportExport\Converters\Webalize(alias, title))
            - call(Wame\ImportExport\Calls\InsertMultipleToDatabase(wame_shop_product, null, false, import_product_id))
            - call(Wame\ImportExport\Calls\InsertMultipleToDatabase(wame_shop_category, null, true))
            - call(App\Model\ImportExport\Configuration\Matterhorn\Relations\ShopProductInCategory())
            - call(Wame\ImportExport\Calls\InsertMultipleToDatabase(wame_shop_product_in_category, null, false, false))
```


## Export

Konfiguracia je definovana v neon suboroch

`App\Model\ImportExport\Export`


### Metody

| metoda                | parametre                                                                 | povinne   | popis                                     |
|-----------------------|---------------------------------------------------------------------------|-----------|-------------------------------------------|
| addSource             | name:string, repository:BaseRepository, criteria:array, indexBy:string    | nie       | Zavola funkciu, ktora pracuje s importom  |
| setOutputRepository   | table:string, db:Nette\Database\Context                                   | ano       | Nastavi vystupny repozitar                |
| map                   | function:callable                                                         | nie       | Zavola funkciu, ktora pracuje s importom  |


### Mapovanie

| nazov         | parametre                                 | popis                                             |
|---------------|-------------------------------------------|---------------------------------------------------|
| `string`      | -                                         | Vrati hodnotu nazaklade vlozeneho kluca           |
| `_ITERATOR_`  | -                                         | Vrati aktualnu hodnotu interatoru, pocinajuc 1    |
| `Map`         | source:string, map:mixed, index:mixed     | Zavola funkciu, ktora pracuje s importom          |


### Priklad

#### Database

```
MoneyCompanyExport:
    class: App\Model\ImportExport\Export
    setup:
        - setOutputRepository(System_XmlExchangeImport, @database.money.context)
        - addSource(user_info, App\Model\User\UserInfoRepository())
        - convert({
            Create_ID: 00000000-0000-0000-0000-000000000000
            KodImportu: ESK01
        })
        - call(App\Model\ImportExport\Converters\ToXml(VstupniXml, {
            FirmaList: {
                Firma: {
                    Kod: null
                    ICO: App\Model\ImportExport\Calls\Map(user_info, ico)
                    DIC: App\Model\ImportExport\Calls\Map(user_info, dic)
                    ICDPH: App\Model\ImportExport\Calls\Map(user_info, ic_dph)
                    SeznamSpojeni: {
                        Spojeni: [
                            {
                                Poradi: 1
                                SpojeniCislo: App\Model\ImportExport\Calls\Map(user_info, phone)
                                TypSpojeni: {
                                    Kod: Tel
                                }
                            },
                            {
                                Poradi: 2
                                SpojeniCislo: veselka@freemail.cz
                                TypSpojeni: {
                                    Kod: E-mail
                                }
                            }
                        ]
                    }
                    Nazev: App\Model\ImportExport\Calls\Map(user_info, companyName)
                    Poznamka: App\Model\ImportExport\Calls\Map(user_info, text)
                }
            }
        }, windows-1252))
        - call(App\Model\ImportExport\Calls\InsertToDatabase())
        - execute()
```

## Napady na zlepsenie

- Zjednotit Import a Export (vznikol nad nimi abstract ale uplne zlucenie je nevyhnutnost)
- Pripravit rozne readery a writery (momentalne je len db2db)