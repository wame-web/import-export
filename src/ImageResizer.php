<?php

namespace Wame\ImportExport;


use GifCreator\GifCreator;
use GifFrameExtractor\GifFrameExtractor;
use Nette\Utils\Finder;
use Nette\Utils\Image;

class ImageResizer
{
    public function resize($original_path, $save_path, $width, $height) {
        if (GifFrameExtractor::isAnimatedGif($original_path)) {
            $this->resizeGif($original_path, $save_path, $width, $height);
        } else {
            $this->resizeNormal($original_path, $save_path, $width, $height);
        }
    }

    public function resizeNormal($original_path, $save_path, $width, $height) {
        //Obycajne obrazky
        $small_image = Image::fromFile($original_path);
        $small_image->resize($width, $height);
        $small_image->sharpen();
        $small_image->save($save_path);
    }

    public function resizeGif($original_path, $save_path, $width, $height) {

        //cesty
        $originalFolder = dirname($original_path);
        $originalName = substr($original_path, 0, strlen($originalFolder + 1));
        $tempFolder = $originalFolder . '/gif_temp';

        if (!is_dir($tempFolder)) {
            @mkdir($tempFolder, 0777, true);
        }

        //load
        $gfe = new GifFrameExtractor();
        $frames = $gfe->extract($original_path);

        //resize
        $retouchedFrames = array();
        $i = 0;
        foreach ($frames as $frame) {

            $tempFile = $tempFolder . '/' . $i . '.gif';

            imagegif($frame['image'], $tempFile);
            unset($frame['image']);

            // Initialization of the frame as a layer
            $frameLayer = Image::fromFile($tempFile);
            $frameLayer->resize($width, $height);
            $frameLayer->sharpen();
            $frameLayer->save($tempFile);

            $retouchedFrames[] = $tempFile;

            $i++;
        }

        unset($frames);

        //save
        $gc = new GifCreator();
        $gc->create($retouchedFrames, $gfe->getFrameDurations(), 0);

        file_put_contents($save_path, $gc->getGif());

        foreach(Finder::find('*')->in($tempFolder) as $file) {
            unlink($file);
        }
    }

}