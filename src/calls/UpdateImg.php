<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\BaseRepository;
use Wame\ImportExport\ImportExport;

class UpdateImg implements Call
{
    /** @var BaseRepository */
    protected $repository;

    /** @var string */
    protected $columnI;

    /** @var string */
    protected $columnO;

    /** @var string */
    protected $columnIdI;

    /** @var string */
    protected $columnIdO;


    function __construct(
        string $columnI,
        string $columnO,
        string $columnIdI,
        string $columnIdO,
        $repository = null
    ) {
        $this->columnI = $columnI;
        $this->columnO = $columnO;
        $this->columnIdI = $columnIdI;
        $this->columnIdO = $columnIdO;
        $this->repository = $repository;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $repository = $this->repository ?? $importExport->getOutputRepository();

        $itemsA = $this->reindex($importExport->getInput());

        $itemsB = $repository->getObjectPairs([], $this->columnIdO);

        $data = [];
        foreach($itemsA as $key => $value) {
            if(!isset($itemsB[$value])) {
                continue;
            }

            $data[] = [
                'import_product_id' => $key,
                $this->columnO => $itemsB[$value]['id'],
                'import_type' => 'money'
            ];
        }

        if(!empty($data)) {
            $importExport->getOutputRepository()->insertOrUpdateMultiple($data);
        }
    }


    private function reindex($input)
    {
        $items = [];

        foreach($input as $item) {
            $items[$item[$this->columnIdI]] = $item[$this->columnI];
        }

        return $items;
    }

}