<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;

class UpdateProductAvailability implements Call
{
    const AVAILABILITY_NOT_AVAILABLE = 1;
    const AVAILABILITY_AVAILABLE = 2;


    public function __invoke(ImportExport &$importExport)
    {
        $items = $importExport->getOutputRepository()->getObjectPairs([]);

        $data = [];
        foreach($items as $item) {
            $data[] = [
                'id' => $item['id'],
                'availability_id' => $item['stock_status'] > 0 ?
                    self::AVAILABILITY_AVAILABLE : self::AVAILABILITY_NOT_AVAILABLE
            ];
        }

        $importExport->getOutputRepository()->insertOrUpdateMultiple($data);
    }

}