<?php

namespace Wame\ImportExport\Calls;

use Tracy\Debugger;
use Wame\ImportExport\ImportExport;

class UpdateStatusByImportType implements Call
{
    /** @var string */
    //private $tableName;
    private $shopCategory;
	private $shopProductInCategory;
    /** @var string */
    //protected $importChangedColumn;

    function __construct(string $shopCategory, string $shopProductInCategory)
    {
		//$this->tableName = $tableName;
		//$this->importChangedColumn = $importChangedColumn;
		$this->shopCategory = $shopCategory;
		$this->shopProductInCategory = $shopProductInCategory;
    }

    public function __invoke(ImportExport &$importExport)
    {
        try {
        	/*
			$importExport->getRepository($this->tableName)
				->update(
					[ 'import_type' => $importExport->getImportType() ],
					[ $this->importChangedColumn => 0 ]
				);
        	*/
        	$categoryIds = $importExport->getRepository($this->shopCategory)->where(array('alias <>' => ''))->select('category_id')->fetchAll();
        	if (count($categoryIds) > 0) {
        		foreach ($categoryIds as $catId) {
					$count = $importExport->getOutputDatabase()->query("SELECT COUNT(product_id) AS xx FROM " . $this->shopProductInCategory
							. " WHERE " . $this->shopProductInCategory . "category_id = ? ",  $catId)->fetch();
					$newStatus = ($count['xx'] > 0 ? 1 : 0);
					$importExport->getRepository($this->shopCategory)->where(array('category_id' => $catId))->update(array('status' => $newStatus));
				}
			}
        } catch(\Exception $exception) {
            Debugger::log($exception);
        }
    }
}
