<?php

namespace Wame\ImportExport\Calls;

use Grido\Exception;
use Tracy\Debugger;
use Wame\ImportExport\ImportExport;


class InsertMultipleToDatabase implements Call
{
    /** @var string */
    private $outputKey;

    /** @var string */
    protected $table;

    /** @var bool */
    protected $unique;

    /** @var string */
    protected $importColumn;

    /** @var array */
    protected $updateWithout;


    function __construct(string $outputKey, string $table = null, bool $unique = false, $importColumn = 'import_id', $updateWithout = [])
    {
        $this->outputKey = $outputKey;
        $this->table = $table ?? $outputKey;
        $this->unique = $unique;
        $this->importColumn = $importColumn;
        $this->updateWithout = $updateWithout;
    }


    public function __invoke(ImportExport &$importExport)
    {
//        Debugger::log("InsertMultipleToDatabase __invoke start - " . $this->table);

        if (isset($importExport->output[$this->outputKey])) {
            $importExport = $this->resortData($importExport);

            $data = $importExport->output[$this->outputKey];

            if ($this->outputKey == 'wame_shop_product') {
//                dump($data);
//                exit;

                // Keď nesedí počet produktov
                if (isset($importExport->input['produkt']) && count($importExport->output[$this->outputKey]) < count($importExport->input['produkt'])) {
                    \Tracy\Debugger::log('Nesedí počet produktov ' . count($importExport->output[$this->outputKey]) . '/' . count($importExport->input['produkt']), 'productcount');
                }
            }



//            dump('InsertMultipleToDatabase');
//            dump($data);

//            try {
//                Debugger::log("InsertMultipleToDatabase __invoke unique: " . $this->unique);
                if ($this->unique) {
                    $data = array_combine(array_column($data, 'alias'), $data);
                }

//                Debugger::log("InsertMultipleToDatabase __invoke after array combine");
                $importExport->report[$this->table] = ['updated' => 0, 'inserted' => 0];
//                Debugger::log("InsertMultipleToDatabase __invoke chunk: " . $importExport->getChunk());
                if ($this->importColumn === false) {
//                    Debugger::log("InsertMultipleToDatabase __invoke before insertOrUpdate");
                    $this->insertOrUpdate($data, $importExport->getChunk(), $importExport);
                } else {
//                    Debugger::log("InsertMultipleToDatabase __invoke before insertAndUpdate");
                    $this->insertAndUpdate($data, $importExport->getChunk(), $importExport);
                }
//                Debugger::log("InsertMultipleToDatabase __invoke after insert/update");
//            } catch (\Exception $exception) {
//                throw new \Exception($exception);
//            } finally {
//                $data = null;
//                unset($data);
//                gc_collect_cycles();
//            }
        } else {
//            Debugger::log('Not found $importExport->output["' . $this->outputKey . '"]');
        }

        $data = null;
        unset($data);
//        if ($this->outputKey != 'wame_shop_product') {
//            unset($importExport->output[$this->outputKey]);
//        }
        gc_collect_cycles();
//        Debugger::log("InsertMultipleToDatabase __invoke end");
    }


    /**
     * Resort data by columnSort
     *
     * @param ImportExport $importExport
     *
     * @return ImportExport
     */
    private function resortData(ImportExport &$importExport)
    {
        if (!isset($importExport->columnSort[$this->table])) return $importExport;

        foreach ($importExport->output[$this->outputKey] as $id => $values) {
            $data = [];

            foreach ($importExport->columnSort[$this->table] as $column) {
                if (array_key_exists($column, $values)) {
                    $data[$column] = $values[$column];
                } elseif (isset($importExport->defaults[$this->table][$column])) {
                    $data[$column] = $importExport->defaults[$this->table][$column];
                }
            }

            foreach ($values as $key => $value) {
                if (!isset($data[$key])) {
                    $data[$key] = $value;
                }
            }

            $importExport->output[$this->outputKey][$id] = $data;
        }

        return $importExport;
    }


    private function insertOrUpdate(array $data, int $chunkSize, ImportExport &$importExport)
    {
//        Debugger::log("InsertMultipleToDatabase insertOrUpdate start");
//        Debugger::log("InsertMultipleToDatabase insertOrUpdate chunkSize: " . $chunkSize);

        foreach (array_chunk($data, $chunkSize) as &$chunk) {
//            Debugger::log("InsertMultipleToDatabase insertOrUpdate foreach");
            echo "InsertMultipleToDatabase insertOrUpdate [" . $this->table . "]\n";
            $importExport->getRepository($this->table)->insertOrUpdateMultiple(array_values($chunk), $this->updateWithout);
        }
//        Debugger::log("InsertMultipleToDatabase insertOrUpdate end");
    }


    private function insertAndUpdate(array $data, int $chunkSize, ImportExport &$importExport)
    {
        $inOutput = array_column($data, $this->importColumn);

//        Debugger::log("InsertMultipleToDatabase insertAndUpdate start - " . $importExport->getImportType());
        $inDb = $importExport->getRepository($this->table)->getPairs(['import_type' => $importExport->getImportType(), $this->importColumn . ' IN (?)' => $inOutput], 'id', $this->importColumn);
//        Debugger::log("InsertMultipleToDatabase insertAndUpdate after inDb");

//        Debugger::log("InsertMultipleToDatabase insertAndUpdate before insert");

//		dumpe($this->getByImportId($data, array_diff($inOutput, $inDb)));


		$this->insert($this->getByImportId($data, array_diff($inOutput, $inDb)), $chunkSize, $importExport);
//        Debugger::log("InsertMultipleToDatabase insertAndUpdate before update");
        //$this->update($this->getByImportId($data, array_diff($inDb, $inOutput)), $chunkSize, $importExport);
        $this->update($data, $chunkSize, $importExport);
        $inDb = null;
        unset($inDb);
        $inOutput = null;
        unset($inOutput);
//        Debugger::log("InsertMultipleToDatabase insertAndUpdate end");

//        Debugger::log("InsertMultipleToDatabase insertAndUpdate start - " . $importExport->getImportType());
//        $inDb = $importExport->getRepository($this->table)->getPairs(['import_type' => $importExport->getImportType()], 'id', $this->importColumn);
//        Debugger::log("InsertMultipleToDatabase insertAndUpdate after inDb");
//        $inOutput = array_column($data, $this->importColumn);
//        Debugger::log("InsertMultipleToDatabase insertAndUpdate before insert");
//        $this->insert($this->getByImportId($data, array_diff($inOutput, $inDb)), $chunkSize, $importExport);
//        Debugger::log("InsertMultipleToDatabase insertAndUpdate before update");
//        //$this->update($this->getByImportId($data, array_diff($inDb, $inOutput)), $chunkSize, $importExport);
//        $this->update($data, $chunkSize, $importExport);
//        $inDb = null;
//        unset($inDb);
//        $inOutput = null;
//        unset($inOutput);
//        Debugger::log("InsertMultipleToDatabase insertAndUpdate end");
    }


    private function insert(array $data, int $chunkSize, ImportExport &$importExport)
    {
//        Debugger::log("InsertMultipleToDatabase insert start");
        $i = 0;
        foreach (array_chunk($data, $chunkSize) as &$chunk) {
//            Debugger::log("InsertMultipleToDatabase insert foreach " . $i);
			echo "InsertMultipleToDatabase insert [" . $this->table . "]\n";
            $importExport->getRepository($this->table)->insert(array_values($chunk));
            $i += count($chunk);
        }
        $importExport->report[$this->table]['inserted'] += $i;
//        Debugger::log("InsertMultipleToDatabase insert end");
    }


    private function update(array $data, int $chunkSize, ImportExport &$importExport)
    {
//        Debugger::log("InsertMultipleToDatabase update start");
        $i = 0;
        foreach (array_chunk($data, $chunkSize) as &$chunk) {
//            Debugger::log("InsertMultipleToDatabase update foreach " . $i);
			//$importExport->getRepository($this->table)->update([], array_values($chunk));
			//$importExport->getRepository($this->table)->insertOrUpdateMultiple(array_values($chunk));
            foreach ($chunk as $values) {
				echo "InsertMultipleToDatabase update [" . $this->table . "]\n";

				$data = [];

				foreach ($values as $k => $v) {
                    if (!in_array($k, $this->updateWithout)) $data[$k] = $v;
                }

				$importExport->getRepository($this->table)->update([$this->importColumn => $values[$this->importColumn], 'import_type' => $importExport->getImportType()], $data);
            }
            $i += count($chunk);
        }
        $importExport->report[$this->table]['updated'] += $i;
//        Debugger::log("InsertMultipleToDatabase update end");
    }


    private function getByImportId($data, $keys)
    {
        return array_filter($data, function($val) use ($keys) {
            return in_array($val[$this->importColumn], $keys);
        });
    }

}
