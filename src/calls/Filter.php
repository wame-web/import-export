<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;

class Filter implements Call
{
    /** @var string */
    protected $key;

    /** @var mixed */
    protected $value;


    function __construct(string $key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }


    public function __invoke(ImportExport &$importExport)
    {
        foreach($importExport->output as $key => &$item) {
            if ($item[$this->key] == $this->value) {
                unset($importExport->output[$key]);
            }
        }
    }

}