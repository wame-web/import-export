<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;

class InsertToDatabase implements Call
{
    public function __invoke(ImportExport &$importExport)
    {
        if(!empty($importExport->getOutput())) {
            foreach (array_chunk($importExport->getOutput(), $importExport->getChunk()) as $chunk) {
                if (!empty($chunk)) {
                    $importExport->getOutputDatabase()->query('INSERT INTO ' . $importExport->getOutputTableName(), $chunk);
                }
            }
        }
    }

}