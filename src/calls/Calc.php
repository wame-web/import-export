<?php

namespace Wame\ImportExport\Calls;

use Nette\Utils\Strings;
use Wame\ImportExport\ImportExport;

class Calc implements Call
{
    /** @var string */
    protected $operator;

    /** @var array */
    protected $values;

    /** @var mixed */
    protected $result;


    function __construct($operator, ...$values)
    {
        $this->operator = $operator;
        $this->values = $values;
    }


    public function __invoke(ImportExport &$importExport, $parent = null)
    {
        $values = $this->values;

        foreach($values as &$value) {
            if(is_callable($value)) {
                $value = call_user_func($value, $importExport);
            } else if(Strings::startsWith($value, '&')) {
                /** @var Map $parent */
                $value = $parent->getValue($importExport, $value);
            }
        }

        return $this->getResult($values);
    }


    private function getResult($vals)
    {
        $this->result= $vals[0];

        $values = array_slice($vals, 1);

        switch($this->operator) {
            case '+': array_walk($values, function($val) { $this->result += $val; }); break;
            case '-': array_walk($values, function($val) { $this->result -= $val; }); break;
            case '*': array_walk($values, function($val) { $this->result *= $val; }); break;
            case '/': array_walk($values, function($val) { $this->result /= $val; }); break;
        }

        return $this->result;
    }

}