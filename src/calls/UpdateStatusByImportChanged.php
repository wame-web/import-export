<?php

namespace Wame\ImportExport\Calls;

use Tracy\Debugger;
use Wame\ImportExport\ImportExport;


class UpdateStatusByImportChanged implements Call
{
    /** @var string */
    private $tableName;

    /** @var string */
    protected $importChangedColumn;


    function __construct(string $tableName, string $importChangedColumn)
    {
        $this->tableName = $tableName;
        $this->importChangedColumn = $importChangedColumn;
    }


    public function __invoke(ImportExport &$importExport)
    {
        echo "UpdateStatusByImportChanged - " . $importExport->getImportType() . "\n";

        try {
            $importExport->getRepository($this->tableName)->update(
            	['import_type' => $importExport->getImportType()],
				[$this->importChangedColumn => 0]
			);
        } catch(\Exception $exception) {
            Debugger::log($exception);
        }
    }

}
