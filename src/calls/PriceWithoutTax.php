<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;

class PriceWithoutTax implements Call
{
    /** @var int */
    protected $round;


    function __construct($round = 4)
    {
        $this->round = $round;
    }


    public function __invoke(ImportExport &$importExport)
    {
        foreach($importExport->output as &$item) {
            $item['price'] = $this->getPriceWithoutTax($item['price_with_tax'], $item['tax'], $this->round);
        }
    }


    /**
     * Get price without tax
     *
     * @param float $priceWithTax price with tax
     * @param float $tax tax
     * @param bool|int $round round precision
     * @return float
     */
    private function getPriceWithoutTax(float $priceWithTax, float $tax, $round = false)
    {
        $priceWithoutTax = $tax ? $priceWithTax / (1 + ($tax / 100)) : $priceWithTax;

        return $round === false ? $priceWithoutTax : round($priceWithoutTax, $round);
    }

}