<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;


class RemoveFile implements Call
{
    public function __invoke(ImportExport &$importExport)
    {
        unlink($importExport->settings['actual_file']);
    }

}
