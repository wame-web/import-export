<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\BaseRepository;
use Wame\ImportExport\ImportExport;

class SaveRelationToDatabase implements Call
{
    /** @var BaseRepository */
    protected $repositoryA;

    /** @var BaseRepository */
    protected $repositoryB;

    /** @var string */
    protected $columnAId;

    /** @var string */
    protected $columnBId;

    /** @var string */
    protected $columnAParentId;

    /** @var string */
    protected $columnBParentId;

    /** @var string */
    protected $columnAImportId;

    /** @var string */
    protected $columnBImportId;


    function __construct(
        BaseRepository $repositoryA,
        BaseRepository $repositoryB,
        string $columnAId,
        string $columnBId,
        string $columnAImportId,
        string $columnBImportId
    ) {
        $this->repositoryA = $repositoryA;
        $this->repositoryB = $repositoryB;
        $this->columnAId = $columnAId;
        $this->columnBId = $columnBId;
        $this->columnAImportId = $columnAImportId;
        $this->columnBImportId = $columnBImportId;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $importExport->getOutputRepository()->truncate();

        $aItems = $this->repositoryA->getPairs([], $this->columnAImportId, 'id');
        $bItems = $this->repositoryB->getPairs([], $this->columnBImportId, 'id');

        $data = [];

        foreach($importExport->getOutput() as $item) {
            if(!isset($aItems[$item[$this->columnAId]]) || !isset($bItems[$item[$this->columnBId]])) continue;

            $data[] = [
                $this->columnAId => $aItems[$item[$this->columnAId]],
                $this->columnBId => $bItems[$item[$this->columnBId]]
            ];
        }

        if(!empty($data)) {
            $importExport->getOutputRepository()->insertOrUpdateMultiple($data);
        }
    }

}