<?php

namespace Wame\ImportExport\Calls;


use Wame\ImportExport\ImportExport;

class StatusChecker implements Call
{
    /** @var ShopOrderStatusRepository */
    protected $shopOrderStatusRepository;


    function __construct($shopOrderStatusRepository)
    {
        $this->shopOrderStatusRepository = $shopOrderStatusRepository;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $itemsId = [];

        foreach($importExport->getOutput() as $item) {
            $itemsId[] = $item->id;
        }

        $itemsId = array_unique($itemsId);

        $itemsInDatabase = $importExport->getOutputRepository()
            ->where(['id' => $itemsId])
            ->fetchPairs('id', 'status');

        foreach($importExport->getOutput() as $item) {
            if(isset($itemsInDatabase[$item->id]) && $item->status != $itemsInDatabase[$item->id]) {
                // store order status history
                $this->storeOrderStatusHistory($itemsInDatabase[$item->id], $item->status);

                // send mail
                $this->shopOrderStatusRepository->changeStatusMail($item->id, $item->status);
            }
        }
    }


    /**
     * Store order status history
     *
     * @param int $oldStatus old status
     * @param int $newStatus new status
     */
    protected function storeOrderStatusHistory(int $oldStatus, int $newStatus)
    {
        $this->shopOrderStatusRepository->insert([
            'status' => $oldStatus,
            'new_status' => $newStatus,
            'update_date' => date('Y-m-d H:i:s'),
            'admin_id' => 2
        ]);
    }

}