<?php

namespace Wame\ImportExport\Calls;

use Nette\Utils\Strings;
use Wame\ImportExport\ImportExport;

class MapRelation implements Call
{
    /** @var string */
    protected $source;

    /** @var string|array */
    protected $map;

    /** @var mixed */
    public $column;

    /** @var mixed */
    public $value;

    /** @var int */
    public $index;

    /** @var string */
    public $col;

    /** @var Map */
    protected $parent;


    function __construct(string $source, $map, $column, $value, $col = 'id')
    {
        $this->source = $source;
        $this->map = $map;
        $this->column = $column;
        $this->value = $value;
        $this->col = $col;
    }


    public function __invoke(ImportExport &$importExport, $parent = null)
    {
        if($this->source == 'shop_product') {
//            Debugger::log("order_item => source: {$this->source} | map: {$this->map} | column: {$this->column} | value: {$this->value} | col: {$this->col}");
        }

        $this->parent = $parent;

        $parentId = $importExport->data[$parent->source][$parent->index][$this->col];

        $this->index = $parent->index; //$parentId

        if(!$this->map) {
            return $this->getValue($importExport);
        }

        if(is_array($this->map)) {
            $temp = $this->map;

            array_walk_recursive($temp, function (&$value, $key) use ($importExport, $parentId) {
                if(is_callable($value)) {
                    $value = call_user_func($value, $importExport, $this);
                }
                else if(is_string($value)) {
                    $words = explode(" ", $value);

                    $newWords = [];

                    foreach($words as $word) {
                        if(Strings::startsWith($word, '&')) {
                            $word = ltrim($word, '&');

                            foreach ($importExport->data[$this->source] as $sourceValue) {
                                if ($sourceValue[$this->column] == $parentId) {
                                    $newWords[] = $sourceValue[$word];
                                    break;
                                }
                            }
                        } else {
                            $newWords[] = $value;
                        }
                    }

                    $value = implode(" ", $newWords);
                }
            });

            return $temp;
        }
    }

    public function getValue(ImportExport $importExport, Map $map = null)
    {
        $parentId = $importExport->data[$this->parent->source][$this->parent->index][$this->col];

        if(is_string($this->value)) {
            $words = explode(" ", $this->value);

            $newWords = [];

            foreach($words as $word) {
                if(Strings::startsWith($word, '&')) {
                    $word = ltrim($word, '&');

                    foreach ($importExport->data[$this->source] as $sourceValue) {
                        if ($sourceValue[$this->column] == $parentId) {
                            $newWords[] = $sourceValue[$word];
                            break;
                        }
                    }
                } else {
                    $newWords[] = $this->value;
                }
            }

            return implode(" ", $newWords);
        }
    }

}