<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\BaseRepository;
use Wame\ImportExport\ImportExport;
use Nette\Database\SqlLiteral;

class UpdateId implements Call
{
    /** @var string */
    private $column;

    /** @var BaseRepository */
    private $repository;

    /** @var string */
    private $tableName;

    /** @var string */
    private $siteIncrementName;

    /** @var mixed */
    private $updateValue;


    public function __construct(string $column, $repository = null, string $tableName, string $siteIncrementName = null, $updateValue = null)
    {
        $this->column = $column;
        $this->repository = $repository;
        $this->tableName = $tableName;
        $this->siteIncrementName = $siteIncrementName;
        $this->updateValue = $updateValue;
    }


    public function __invoke(ImportExport &$importExport)
    {
//        \Tracy\Debugger::log('UpdateId');
        echo "UpdateId - " . $importExport->getImportType() . "\n";

        $this->updateTableId($importExport);
        $this->updateSiteIncrement($importExport);
    }


    /**
     * Update table ID
     *
     * @param ImportExport $importExport
     */
    private function updateTableId(ImportExport &$importExport)
    {
        $repository = $this->repository ?? $importExport->getOutputRepository();
        $repository->update([$this->column => $this->updateValue], [$this->column => new SqlLiteral('id')]);
    }

    /**
     * Update site increment
     *
     * @param ImportExport $importExport
     */
    private function updateSiteIncrement(ImportExport &$importExport)
    {
        $count = $importExport->report[$this->tableName]['inserted'];

        $siteRepository = $importExport->getRepository('wame_site_settings');
        $incrementId = $siteRepository->findOneBy(['name' => $this->siteIncrementName]);
        $incrementId = $incrementId['value'];
        $incrementId += $count; // TODO: get inserted rows
        $siteRepository->update(['name' => $this->siteIncrementName], ['value' => $incrementId]);
    }

}