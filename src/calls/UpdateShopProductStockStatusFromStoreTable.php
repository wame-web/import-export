<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;


/**
 * Class UpdateShopProductStockStatusFromStoreTable
 *
 * Aktualizuje stav skladu z tabuľky `wame_shop_store_product` na produktoch v tabuľke `wame_shop_product` stĺpec `stock_status`
 *
 * @package Wame\ImportExport\Calls
 */
class UpdateShopProductStockStatusFromStoreTable implements Call
{
    public function __invoke(ImportExport &$importExport)
    {
        // Vytiahne IDčka aktívnych skladov
        $activeStores = $importExport->getRepository('wame_shop_store')->getPairs(['status' => 1], 'id', 'id');

        $query = "UPDATE wame_shop_product AS p "
            . "LEFT JOIN (SELECT product_id, SUM(quantity) AS sum_store FROM wame_shop_store_product WHERE store_id IN (?) GROUP BY product_id) AS s ON p.product_id = s.product_id "
            . "SET p.stock_status = s.sum_store ";

        $importExport->getRepository('wame_shop_product')->db->query($query, array_keys($activeStores));
    }

}
