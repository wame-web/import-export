<?php

namespace Wame\ImportExport\Calls;

use Tracy\Debugger;
use Wame\ImportExport\ImportExport;


class UpdateStatuses implements Call
{
    /** @var string */
    private $shopCategory;
	private $shopProduct;
	private $shopProductInCategory;


    function __construct(string $shopCategory, string $shopProductInCategory, string $shopProduct)
    {
		$this->shopCategory = $shopCategory;
		$this->shopProductInCategory = $shopProductInCategory;
		$this->shopProduct = $shopProduct;
    }


    public function __invoke(ImportExport &$importExport)
    {
        try {
			$shopCategory = $importExport->getRepository($this->shopCategory);
			$shopProductInCategory = $importExport->getRepository($this->shopProductInCategory);
			$shopProduct = $importExport->getRepository($this->shopProduct);
			//$categoryIds = $shopCategory->db->query("SELECT category_id AS id FROM " . $this->shopCategory . " WHERE alias <> ? ORDER BY category_id ASC", '')->fetchAll();
			$categoryIds = $shopCategory->db->query("SELECT category_id AS id FROM " . $this->shopCategory . " WHERE import_type = ? ORDER BY category_id ASC", $importExport->getImportType())->fetchAll();

            echo "UpdateStatuses " . $importExport->getImportType() . " - " . count($categoryIds) . "\n";

        	if (count($categoryIds) > 0) {
				$categoryIdsPos = [];
				$categoryIdsNeg = [];

				foreach ($categoryIds as $catId) {
					$productIds = $shopProductInCategory->db->query("SELECT product_id FROM " . $this->shopProductInCategory . " WHERE category_id = ?", $catId['id'])->fetchAll();

					if (count($productIds) > 0) {
						$count = $shopProduct->db->query("SELECT COUNT(product_id) AS xx FROM " . $this->shopProduct . " WHERE `status` = 1 AND product_id IN (?)", array_values($productIds))->fetch();

						if ($count['xx'] > 0) {
							$categoryIdsPos[] = $catId['id'];
						} else {
							$categoryIdsNeg[] = $catId['id'];
						}
					} else {
						$categoryIdsNeg[] = $catId['id'];
					}
					/*
					$count = $shopProductInCategory->db->query("SELECT COUNT(product_id) AS xx FROM " . $this->shopProductInCategory . " WHERE category_id=" . $catId['id'])->fetch();
					if ($count['xx'] > 0) {
						$categoryIdsPos[] = $catId['id'];
					} else {
						$categoryIdsNeg[] = $catId['id'];
					}
					*/
				}

				if (count($categoryIdsPos) > 0) {
					$shopCategory->db->query("UPDATE ". $this->shopCategory . " SET `status` = 1 WHERE category_id IN (?)", $categoryIdsPos);
				}

				if (count($categoryIdsNeg) > 0) {
					$shopCategory->db->query("UPDATE ". $this->shopCategory . " SET `status` = 0 WHERE category_id IN (?)", $categoryIdsNeg);
				}
        	}
        } catch(\Exception $exception) {
            Debugger::log($exception);
        }
    }

}
