<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;

class UpdateParent implements Call
{
    /** @var string */
    protected $column;

    /** @var string */
    protected $importId;

    /** @var string */
    protected $importParentId;


    function __construct(string $column, string $importId = 'import_id', string $importParentId = 'import_parent_id')
    {
        $this->column = $column;
        $this->importId = $importId;
        $this->importParentId = $importParentId;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $items = $importExport->getOutputRepository()->getObjectPairs([], $this->importId);

        $data = [];
        foreach($items as $item) {
            if(!isset($items[$item[$this->importParentId]])) {
                continue;
            }

            $data[] = [
                'id' => $item['id'],
                $this->column => $items[$item[$this->importParentId]]['id']
            ];
        }

        $importExport->getOutputRepository()->insertOrUpdateMultiple($data);
    }

}