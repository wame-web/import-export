<?php

namespace Wame\ImportExport\Calls;

use App\Model\Shop\Store\ShopStoreProductRepository;
use Tracy\Debugger;
use Wame\ImportExport\ImportExport;


class UpdateStore implements Call
{
    const TABLE_PRODUCT = 'wame_shop_product';


    /** @var ShopStoreProductRepository */
    private $shopStoreProductRepository;


    public function __construct(ShopStoreProductRepository $shopStoreProductRepository)
    {
        $this->shopStoreProductRepository = $shopStoreProductRepository;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $logName = $importExport->getImportType() . " | ";

        echo $logName . 'start';
//        Debugger::log($logName . 'start');

        $database = $importExport->getInputDatabase();

        $productIds = [];

        if (isset($importExport->output['wame_shop_store_product'])) {
            $productIds = array_column($importExport->output['wame_shop_store_product'], 'product_id');
        }

        if (count($productIds) == 0 && isset($importExport->output['wame_shop_product'])) {
            $productIds = array_column($importExport->output['wame_shop_product'], 'product_id');
        }

        if (count($productIds) == 0) {
            $list = $database->table('wame_shop_store_product')->fetchAll();
        } else {
            $list = $database->table('wame_shop_store_product')->where(['product_id' => $productIds])->fetchAll();
        }

        $data = [];

        foreach ($list as $row) {
            if (isset($data[$row['product_id']])) {
                $data[$row['product_id']] = $row['quantity'] + $data[$row['product_id']];
            } else {
                $data[$row['product_id']] = $row['quantity'];
            }
        }

        if (count($data) > 0) {
            foreach ($data as $productId => $quantity) {
                $database->table(self::TABLE_PRODUCT)->where('product_id', $productId)->update(['stock_status' => $quantity, 'max_purchase' => $quantity]);
            }

            $this->updateMainProducts($database, $data);
        }

//        Debugger::log($logName . 'end');
    }


    private function updateMainProducts($database, $data)
    {
//        Debugger::log('updateMainProducts');

        $products = $database->table(self::TABLE_PRODUCT)->where(['product_id IN (?)' => array_keys($data), 'descendant_product != ?' => 0, 'status' => 2])->fetchAll();

        if (count($products) > 0) {
            $list = [];

            foreach ($products as $product) {
                if (isset($list[$product['descendant_product']])) {
                    $list[$product['descendant_product']] = $product['stock_status'] + $list[$product['descendant_product']];
                } else {
                    $list[$product['descendant_product']] = $product['stock_status'];
                }
            }

//            $database->query('START TRANSACTION');

            foreach ($list as $productId => $quantity) {
//                $database->table(self::TABLE_PRODUCT)->where('product_id', $productId)->update(['stock_status' => $quantity, 'max_purchase' => $quantity]);
                $this->shopStoreProductRepository->mainProductProcess($productId);
            }

//            $database->query('COMMIT');
        }
    }

}
