<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;

class SaveToDatabase implements Call
{
    public function __invoke(ImportExport &$importExport)
    {
//        $this->clearOutput($importExport);

        foreach(array_chunk($importExport->getOutput(), $importExport->getChunk()) as $chunk) {
            $importExport->getOutputRepository()->insertOrUpdateMultiple($chunk);
        }
    }


    /**
     * Clear output
     *
     * Remove all keys which are not match column in database
     *
     * @param $importExport
     */
    protected function clearOutput(&$importExport) // TODO: not working yet
    {
        $columns = $importExport->getOutputRepository()->getTableColumns();
        $keys = array_keys($importExport->getOutput()[0]);

        foreach ($keys as $key) {
            if (!in_array($key, $columns)) {
                unset($importExport->output[$key]);
            }
        }
    }

}