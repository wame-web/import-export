<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;


class RemoveIfMissedKey implements Call
{
    /**
     * null = input
     * string = output table name
     *
     * @var string|null
     */
    private $output;

    /**
     * Key name
     *
     * @var string
     */
    protected $key;


    function __construct(string $output = null, string $key)
    {
        $this->output = $output;
        $this->key = $key;
    }


    public function __invoke(ImportExport &$importExport)
    {
        if ($this->output) {
            foreach ($importExport->output[$this->output] as $key => $values) {
                if (!isset($values[$this->key])) {
                    unset($importExport->output[$this->output][$key]);
                }
            }
        } else {
            foreach ($importExport->getInput() as $inputName => $rows) {
                foreach ($rows as $key => $values) {
                    if (!isset($values[$this->key])) {
                        unset($importExport->input[$inputName][$key]);
                    }
                }
            }
        }
    }

}
