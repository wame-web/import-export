<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;
use Spatie\ArrayToXml\ArrayToXml;

class ToXml implements Call
{
    const ENCODING_UTF8 = 'UTF-8';
    const ENCODUNG_WINDOWS1252 = 'Windows-1252';


    /** @var array */
    public $data = [];

    /** @var string */
    public $key;

    /** @var string */
    public $encoding;


    function __construct(string $key, array $data, $encoding = self::ENCODING_UTF8)
    {
        $this->data = $data;
        $this->key = $key;
        $this->encoding = $encoding;
    }


    public function __invoke(ImportExport &$importExport)
    {
        foreach ($importExport->output as $key => &$item) {
            array_walk_recursive($this->data, function (&$value, $key) use ($importExport, &$data) {
                if (is_callable($value)) {
                    $value = call_user_func($value, $importExport);
                }
            });

            if ($this->emptyRecursive($this->data)) {
                unset($importExport->output[$key]);
            } else {
                $data = $this->data;

                $dom = (new ArrayToXml($data, $this->key))->toDom();
                $dom->encoding = $this->encoding;
                $xml = $dom->saveXML();
                $item[$this->key] = mb_convert_encoding($xml, $this->encoding, self::ENCODING_UTF8);
                //            $item[$this->key] = iconv(self::ENCODING_UTF8, $this->encoding, $xml);
            }
        }
    }


    /**
     * Check recursively if empty array
     *
     * @param mixed $value value
     * @return bool
     */
    function emptyRecursive($value)
    {
        if (is_array($value)) {
            $empty = true;

            array_walk_recursive($value, function($item) use (&$empty) {
                $empty = $empty && empty($item);
            });
        } else {
            $empty = empty($value);
        }

        return $empty;
    }

}