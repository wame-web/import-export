<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;

class UpdateProductParent extends UpdateParent
{
    public function __invoke(ImportExport &$importExport)
    {
        $items = $importExport->getOutputRepository()->getObjectPairs([], $this->importId);

        $data = [];
        foreach($items as $item) {
            if(!isset($items[$item[$this->importParentId]])) {
                continue;
            }

            $status = $item['status'];
            if($status == 1) $status = 2;
            else if($status == 0) $status = 5;

            $data[] = [
                'id' => $item['id'],
                'status' => $status,
                $this->column => $items[$item[$this->importParentId]]['id']
            ];
        }

        $importExport->getOutputRepository()->insertOrUpdateMultiple($data);
    }

}