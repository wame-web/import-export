<?php

namespace Wame\ImportExport\Calls;

use Nette\Database\SqlLiteral;
use Wame\ImportExport\ImportExport;


/**
 * Class UpdateMainProductAvailabilityFromDescenants
 *
 * Aktualizuje `availability_id` hlavných produktoch podľa variant
 *
 * @package Wame\ImportExport\Calls
 */
class UpdateMainProductAvailabilityFromDescenants implements Call
{
    public function __invoke(ImportExport &$importExport)
    {
        $importExport->getRepository('wame_shop_product')->updateMainProductAvailability();
    }

}
