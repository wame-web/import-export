<?php

namespace Wame\ImportExport\Calls;

use Nette\Database\SqlLiteral;
use Wame\ImportExport\ImportExport;


/**
 * Class UpdateShopProductStoreMainProducts
 *
 * Aktualizuje stav skladov hlavných produktov v tabuľke `wame_shop_store_product`
 *
 * @package Wame\ImportExport\Calls
 */
class UpdateShopProductStoreMainProducts implements Call
{
    public function __invoke(ImportExport &$importExport)
    {
        $stores = $importExport->getRepository('wame_shop_store')->getPairs([], 'id', 'id');

        foreach ($stores as $storeId) {
            $query = "UPDATE wame_shop_store_product AS sp "
                . "LEFT JOIN wame_shop_product AS p ON p.product_id = sp.product_id "
                . "LEFT JOIN (SELECT jp.descendant_product AS main_product, jsp.product_id, SUM(jsp.quantity) AS sum_store "
                    . "FROM wame_shop_store_product AS jsp "
                    . "LEFT JOIN wame_shop_product AS jp ON jp.product_id = jsp.product_id "
                    . "WHERE jsp.store_id = ? GROUP BY jp.descendant_product) AS s ON sp.product_id = s.main_product "
                . "SET sp.quantity = s.sum_store "
                . "WHERE p.descendant_product = ? AND s.sum_store IS NOT NULL AND sp.store_id = ?";

            $importExport->getRepository('wame_shop_product')->db->query($query, $storeId, 0, $storeId);
        }
    }

}
