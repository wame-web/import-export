<?php

namespace Wame\ImportExport\Calls;

use Tracy\Debugger;
use Wame\ImportExport\ImportExport;


class UpdateAliasByParent implements Call
{
    /** @var string */
    private $table;

    /** @var string */
    private $parentColumn;


    public function __construct($table, $parentColumn)
    {
        $this->table = $table;
        $this->parentColumn = $parentColumn;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $logName = $importExport->getImportType() . " | UpdateAliasByParent | ";

        echo $logName . 'start';
//        Debugger::log($logName . 'start');

        $items = $importExport->output[$this->table];

        ksort($items);

        foreach ($items as $key => $data) {
            if ($data[$this->parentColumn] == '' || $data[$this->parentColumn] == null) continue;
            if (!isset($items[$data[$this->parentColumn]])) continue;

            $alias = $importExport->output[$this->table][$key]['alias'];

            $importExport->output[$this->table][$key]['alias'] = $items[$data[$this->parentColumn]]['alias'] . '/' . $alias;
        }

//        Debugger::log($logName . 'end');
    }

}
