<?php

namespace Wame\ImportExport\Calls;

use Tracy\Debugger;
use Wame\ImportExport\ImportExport;


/**
 * Class UpdateShopProductStockStatusFromDescendantsStockStatus
 *
 * Aktualizuje stav skladu z variant na hlavných produktoch
 *
 * @package Wame\ImportExport\Calls
 */
class UpdateShopProductStockStatusFromDescendantsStockStatus implements Call
{
    public function __invoke(ImportExport &$importExport)
    {
        $database = $importExport->getRepository('wame_shop_product')->db;

//        $findTransactions = $database->query('SELECT * FROM `information_schema`.`innodb_trx` ORDER BY `trx_started`')->fetchAll();
//
//        if ($findTransactions) {
//            foreach ($findTransactions as $transaction) {
//                $database->query('KILL ' . $transaction['trx_mysql_thread_id']);
//            }
//        }

        $database->query("SET FOREIGN_KEY_CHECKS=0");
        $database->query('START TRANSACTION');

        try {
            $query = "UPDATE wame_shop_product AS p "
                . "JOIN (SELECT descendant_product, SUM(stock_status) AS sum_stock_status FROM wame_shop_product WHERE descendant_product > ? AND descendant_product IS NOT NULL AND status = ? GROUP BY descendant_product) AS d ON p.product_id = d.descendant_product "
                . "SET p.stock_status = d.sum_stock_status "
                . "WHERE p.descendant_product = ?";

            $database->query($query, 0, 2, 0);

            $database->query('COMMIT');
            $database->query("SET FOREIGN_KEY_CHECKS=1");
        } catch (\Exception $e) {
            Debugger::log($e);
            Debugger::log('EXCEPTION - ' . $e->getMessage());
            $database->query('ROLLBACK');
            $database->query("SET FOREIGN_KEY_CHECKS=1");
        }
    }

}
