<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;

class Condition implements Call
{
    /** @var mixed */
    protected $expression;

    /** @var array */
    protected $statements;

    /** @var Map */
    protected $parent;


    function __construct($expression, array $statements)
    {
        $this->expression = $expression;
        $this->statements = $statements;
    }


    public function __invoke(ImportExport &$importExport, $parent = null)
    {
        $this->parent = $parent;

        $expression = $this->expression;

        if(is_callable($expression)) {
            $expression = call_user_func($expression, $importExport, $this->parent);
        }

        $statement = $this->statements[$expression];

        if($statement) {
            if (is_callable($statement)) {
                $statement = call_user_func($statement, $importExport, $this->parent);
            }

            return $statement;
        }

        // TODO: add default
    }

}