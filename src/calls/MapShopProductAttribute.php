<?php

namespace Wame\ImportExport\Calls;

use Nette\Neon\Neon;
use Wame\ImportExport\ImportExport;


class MapShopProductAttribute implements Call
{
    const TABLE = 'wame_shop_product_attribute';

    /** @var string */
    private $inputKey;


    public function __construct($inputKey = 'plu')
    {
        $this->inputKey = $inputKey;
    }


    public function __invoke(ImportExport &$importExport)
    {
//        \Tracy\Debugger::log('MapShopProductAttribute');
        if (!isset($importExport->output[self::TABLE])) return;

        $settings = Neon::decode($importExport->getClass()->getSettings()['attribute_map']);
        $attributeList = $importExport->getRepository('wame_shop_product_attribute_settings')->getPairs([], 'attribute_id', 'name');

        foreach ($importExport->output[self::TABLE] as $index => $item) {
            if (isset($settings[$item['import_category']])) {
//                dump($index);
//                dump($item);
//                dump(array_search($index, array_keys($importExport->output['wame_shop_product'])));
//                dump($importExport->input[$this->inputKey]);
//                exit;

                $input = $importExport->input['plu'][array_search($index, array_keys($importExport->output['wame_shop_product']))];
                $importExport->output[self::TABLE][$index] = $this->prepareItem($importExport, $settings[$item['import_category']], $attributeList, $item, $input);
            } else {
                unset($importExport->output[self::TABLE][$index]);
            }
        }
    }


    private function prepareItem($importExport, $settings, $attributeList, $item, $input)
    {
        $settings = array_flip($settings);

        $return = ['shop_products_id' => $item['shop_products_id']];

        foreach ($attributeList as $attributeId => $attributeName) {
            if (isset($input[$settings[$attributeId]])) {
                $return[$attributeName] = $input[$settings[$attributeId]];
            } else {
                $return[$attributeName] = null;
            }
        }

        return $return;
    }

}