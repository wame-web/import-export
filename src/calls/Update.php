<?php

namespace Wame\ImportExport\Calls;

use App\Model\BaseRepository;
use Wame\ImportExport\ImportExport;


class Update implements Call
{
    /** @var string */
    private $table;

    /** @var array */
    private $condition = [];

    /** @var array */
    private $update;

    /** @var bool */
    private $onlyChunk;

    /** @var string */
    private $column;


    public function __construct($table, array $condition, array $update, $onlyChunk = false, $column = 'import_id')
    {
        $this->table = $table;
        $this->condition = $condition;
        $this->update = $update;
        $this->onlyChunk = $onlyChunk;
        $this->column = $column;
    }


    public function __invoke(ImportExport &$importExport)
    {
//        \Tracy\Debugger::log('Update');
        if ($this->onlyChunk === true) {
            $this->condition[$this->column] = array_column($importExport->output[$this->table], $this->column);
        }

//        \Tracy\Debugger::log([$this->table, $this->condition, $this->update]);
        echo 'Update - ' . $this->table;

        $importExport->getRepository($this->table)->update($this->condition, $this->update);
    }

}
