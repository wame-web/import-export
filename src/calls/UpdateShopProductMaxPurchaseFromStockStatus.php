<?php

namespace Wame\ImportExport\Calls;

use Nette\Database\SqlLiteral;
use Wame\ImportExport\ImportExport;


/**
 * Class UpdateShopProductMaxPurchaseFromStockStatus
 *
 * Aktualizuje `max_purchase` stĺpec hodnotou `stock_status` daného produktu
 *
 * @package Wame\ImportExport\Calls
 */
class UpdateShopProductMaxPurchaseFromStockStatus implements Call
{
    public function __invoke(ImportExport &$importExport)
    {
        $importExport->getRepository('wame_shop_product')->update(['import_type' => $importExport->getImportType()], ['max_purchase' => new SqlLiteral('stock_status')]);
    }

}
