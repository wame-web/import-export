<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;


class CopyPriceToMainProduct
{
    public function __invoke(ImportExport $importExport)
    {
        $mainProductIds = $importExport->getRepository('wame_shop_product')->getPairs(['descendant_product != ? AND descendant_product IS NOT NULL' => 0], 'descendant_product', 'descendant_product');

        foreach (array_chunk($mainProductIds, 500, true) as $chunk) {
            $products = $importExport->getRepository('wame_shop_product')->getPairs(['descendant_product IN (?)' => array_keys($chunk)], 'product_id', 'descendant_product');


            foreach ($chunk as $key) {
                unset($mainProductIds[$key]);
            }

            $prices = $importExport->getRepository('wame_shop_product_price')->getObjectPairs(['shop_product_id IN (?)' => array_keys($products), 'status' => 1], 'shop_product_id');

            $minPrices = $this->findMinPrices($products, $prices);

            $products = null;
            $prices = null;

            if (count($minPrices) > 0) {
                $importExport->getRepository('wame_shop_product_price')->insertOrUpdateMultiple($minPrices);
            }

            $minPrices = null;
        }

        $mainProductIds = null;
    }


    private function preparePriceIds($data)
    {
        $return = [];

        foreach ($data as $price) {
            $return[] = $price['shop_product_id'];
        }

        return $return;
    }


    private function findMinPrices($products, $prices)
    {
        $return = [];
        $date = date('Y-m-d H:i:s');

        foreach ($products as $productId => $descendantProductId) {
            if (!isset($prices[$productId])) continue;
            if (isset($return[$descendantProductId]) && $return[$descendantProductId]['price_with_tax'] <= $prices[$productId]['price_with_tax']) continue;

            $data = $this->prepareData($prices[$productId]);
            $data['shop_product_id'] = $descendantProductId;
            $data['create_date'] = $date;
            $data['status'] = 1;
            $data['import_id'] = $descendantProductId;
            $data['import_type'] = $this->importExport->getImportType();

            $return[$descendantProductId] = $data;

            unset($products[$productId]);
            unset($prices[$productId]);
        }

        return $return;
    }


    private function prepareData($data)
    {
        $return = [];

        foreach ($data as $key => $value) {
            if (in_array($key, ['id', 'shop_product_id', 'import_id', 'import_type'])) continue;

            $return[$key] = $value;
        }

        return $return;
    }

}
