<?php

namespace Wame\ImportExport\Calls;

use Nette\Utils\Image;
use Tracy\Debugger;
use Wame\ImportExport\ImportExport;

class SaveFile implements Call
{
    const FOLDER_SHOP = IMAGES_PATH . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'product'; // TODO: IMAGES_PATH

    /** @var array */
    protected $sizes = [];


    function __construct($sizes = [])
    {
        $this->sizes = $sizes;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $this->createImages($importExport);
    }


    /**
     * Create images
     *
     * @param $import
     */
    private function createImages(ImportExport $import)
    {
        $images = $import->getOutputRepository()
            ->getTable()
            ->where('edit_date > ?', $import->settings['date'])
            ->fetchPairs('import_id');

        foreach (array_chunk($images, 100, true) as $chunk) {
            $filtered = array_filter($chunk);
            $repository = clone $import->getInputRepository();
            $items = $repository->where('ID', array_keys($filtered))->select('ID, Nazev, Dokument')->fetchAll();

            foreach ($items as $item) {
                try {
                    if(!$item['Dokument']) continue;

                    $image = $images[$item['ID']];
                    $shopProductId = $image['shop_product_id'];
                    $imageId = $image['id'];
                    $imageName = $image['image_name'];

                    $pathOriginal = self::FOLDER_SHOP . DIRECTORY_SEPARATOR . $shopProductId . DIRECTORY_SEPARATOR . 'original';
                    $this->createDir($pathOriginal);

                    $this->createFile($pathOriginal, $imageName, $item['Dokument']); // driver: dblib
//                    $this->createFile($pathOriginal, $imageName, hex2bin($item['Dokument'])); // driver: sqlsrv

                    foreach($this->sizes as $name => $size) {
                        $pathResize = self::FOLDER_SHOP . DIRECTORY_SEPARATOR . $shopProductId . DIRECTORY_SEPARATOR . $name;
                        $this->createDir($pathResize);
//                        $imageResizer->resize($pathOriginal . DIRECTORY_SEPARATOR . $imageName, $pathResize . DIRECTORY_SEPARATOR . $imageName, $size[0], $size[1]);
                        $this->resize($pathOriginal . DIRECTORY_SEPARATOR . $imageName, $pathResize . DIRECTORY_SEPARATOR . $imageName, $size[0], $size[1]);
                    }
                } catch(\Exception $ex) {
//                    Debugger::log($ex->getMessage());
                }

                set_time_limit(240);
                gc_collect_cycles();
            }
        }
    }


    /**
     * Create directories
     *
     * @param array $names names of directories
     */
    private function createDirs(array $names)
    {
        foreach($names as $name) {
            $this->createDir($name);
        }
    }

    /**
     * Create directory
     *
     * @param string $name directory name
     */
    private function createDir(string $name, $mode = 0777)
    {
        if (!file_exists($name)) {
            mkdir($name, $mode, true);
        } else {
            chmod($name, $mode);
        }
    }

    /**
     * Create file
     *
     * @param string $path path
     * @param string $name file name
     * @param mixed $data content
     */
    private function createFile(string $path, string $name, $data)
    {
        file_put_contents($path . DIRECTORY_SEPARATOR . $name, $data);
    }

    /**
     * Resize
     *
     * @param string $originalPath original path
     * @param string $savePath save path
     * @param int $width width
     * @param int $height height
     */
    private function resize(string $originalPath, string $savePath, int $width, int $height)
    {
        $image = Image::fromFile($originalPath);
        $image->alphaBlending(true);
        $image->resize($width, $height, Image::SHRINK_ONLY);
        $image->alphaBlending(false);
        $image->saveAlpha(true);
//        $image->sharpen();
        $image->save($savePath, 9, Image::PNG);
    }

}