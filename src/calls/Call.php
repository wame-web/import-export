<?php

namespace Wame\ImportExport\Calls;


use Wame\ImportExport\ImportExport;

interface Call
{
    public function __invoke(ImportExport &$importExport);

}