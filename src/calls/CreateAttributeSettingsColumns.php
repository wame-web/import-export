<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\BaseRepository;
use Wame\ImportExport\ImportExport;

class CreateAttributeSettingsColumns implements Call
{
    /** @var BaseRepository */
    protected $repository;


    function __construct(BaseRepository $repository)
    {
        $this->repository = $repository;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $tableName = $this->repository->getTableName();

        foreach($importExport->getOutput() as $item) {
            $this->repository->db->query("ALTER TABLE `{$tableName}` ADD COLUMN IF NOT EXISTS `{$item['name']}` VARCHAR(127)");
        }
    }

}