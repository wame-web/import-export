<?php

namespace Wame\ImportExport\Calls;

use Nette\Utils\Strings;
use Wame\ImportExport\ImportExport;

class Map implements Call
{
    /** @var string */
    public $source;

    /** @var string|array */
    protected $map;

    /** @var mixed */
    public $index;

    /** @var Map */
    protected $parent;


    function __construct(string $source, $map, $index = 0)
    {
        $this->source = $source;
        $this->map = $map;
        $this->index = $index;
    }


    public function __invoke(ImportExport &$importExport, Map $parent = null, $index = 0)
    {
        $this->parent = $parent;

        if(is_string($this->map)) {
            return $this->getValue($importExport, $this->map);
        } else if(is_array($this->map)) {
            $data = [];
            $this->index = $index;

            foreach ($importExport->data[$this->source] as $key => $sourceValue) {
                $temp = $this->map;

                array_walk_recursive($temp, function(&$value, $key) use ($importExport, $sourceValue) {
                    if(is_callable($value)) {
                        $value = call_user_func($value, $importExport, $this, $this->index);
                    } else if($value == '_ITERATOR_') {
                        $value = $this->index;
                    } else if(is_string($value)) {
                        // TODO: improve using regular expression

                        $words = explode(" ", $value);
                        $newWords = [];

                        foreach($words as $word) {
                            if(Strings::startsWith($word, '&')) {
                                $newWords[] = $sourceValue[ltrim($word, '&')];
                            } else {
                                $newWords[] = $value;
                            }
                        }

                        $value = implode(" ", $newWords);
                    } else if(is_array($value)) { // TODO: otestovat ci vie spajat pole
                        $newVals = [];

                        foreach($value as $val) {
                            if(is_callable($val)) {
                                $newVals[] = call_user_func($val, $importExport, $this, $this->index);
                            }
                        }

                        $value = implode(" ", $newVals);
                    }
                });

                $data[] = $temp;

                $this->index++;
            }

            return $data;
        }
    }


    /**
     * Get value
     *
     * @param ImportExport $importExport import export
     * @param Map $map map
     * @return null
     */
    public function getValue(ImportExport $importExport, Map $map)
    {
        if($this->index == '_ITERATOR_' && $this->parent) {
            $this->index = $this->parent->index;
        }

        if(Strings::startsWith($map, '&')) {
            $map = ltrim($map, '&');
        }

        return $importExport->data[$this->source][$this->index][$map] ?? null; // TODO: data, nema byt output?
    }

}