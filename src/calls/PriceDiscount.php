<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;

class PriceDiscount implements Call
{
    /** discount type */
    const DISCOUNT_TYPE_PERCENTAGE = 1;
    const DISCOUNT_TYPE_ABSOLUTE = 2;


    /** @var string */
    protected $type;

    /** @var int */
    protected $round;


    function __construct($type = self::DISCOUNT_TYPE_ABSOLUTE, $round = 4)
    {
        switch($type) {
            case 'percentage':
                $this->type = self::DISCOUNT_TYPE_PERCENTAGE;
                break;
            case 'absolute':
            default:
                $this->type = self::DISCOUNT_TYPE_ABSOLUTE;
                break;
        }

        $this->round = $round;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $pricesOld = $importExport->getOutputRepository()
            ->getTable()
            ->where(['status' => 0])
            ->select('shop_product_id, price, price_with_tax')
            ->order('create_date DESC')
            ->fetchPairs('shop_product_id');

        $pricesNew = $importExport->getOutputRepository()
            ->findBy(['status' => 1, 'create_date >' => $importExport->settings['date']], 'create_date DESC')
            ->select('id, shop_product_id, tax, price, price_with_tax');

        $data = [];

        foreach($pricesNew as $price) {
            if(!isset($pricesOld[$price->shop_product_id])) continue;

            $temp = [];

            $priceOld = $pricesOld[$price->shop_product_id];

            $temp['id'] = $price->id;

            if($price->price_with_tax < $priceOld->price_with_tax) {
                switch($this->type) {
                    case self::DISCOUNT_TYPE_ABSOLUTE:
                        $temp['discount'] = $this->getAbsoluteChange($price->price_with_tax, $priceOld->price_with_tax);
                        break;
                    case self::DISCOUNT_TYPE_PERCENTAGE:
                        $temp['discount'] = $this->getPercentageChange($price->price_with_tax, $priceOld->price_with_tax);
                        break;
                }

                $temp['discount_type'] = $this->type;
                $temp['before_discount_price'] = $priceOld->price;
                $temp['before_discount_price_with_tax'] = $priceOld->price_with_tax;
            } else {
                $temp['discount'] = 0;
                $temp['discount_type'] = self::DISCOUNT_TYPE_ABSOLUTE;
                $temp['before_discount_price'] = null;
                $temp['before_discount_price_with_tax'] = null;
            }

            $data[] = $temp;
        }

        if(!empty($data)) {
            $importExport->getOutputRepository()->insertOrUpdateMultiple($data);
        }
    }


    /**
     * Get percentage change
     *
     * @param float $new new price
     * @param float $old old price
     * @return float
     */
    protected function getPercentageChange(float $new, float $old)
    {
        return (float) number_format((1 - $new / $old) * 100, $this->round);
    }

    /**
     * Get absolute change
     *
     * @param float $new new price
     * @param float $old old price
     * @return float
     */
    protected function getAbsoluteChange(float $new, float $old)
    {
        return round(($old - $new), $this->round);
    }

}