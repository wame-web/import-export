<?php

namespace Wame\ImportExport\Calls;

use Wame\ImportExport\ImportExport;


class ResetProgress implements Call
{
    /**
     * import or export
     *
     * @var string
     */
    private $type;


    public function __construct($type)
    {
        $this->type = $type;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $importExport->getInputDatabase()->table('wame_import_export_settings')->where(['name' => $this->type . ucfirst($importExport->getImportType()) . '_progress'])->update(['value' => 0]);
    }

}
