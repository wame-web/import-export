<?php

namespace Wame\ImportExport\EndCalls;

use Tracy\Debugger;
use Wame\ImportExport\Calls\Call;
use Wame\ImportExport\ImportExport;


/**
 * Class DescendantProduct
 *
 * Nastaví descendant product produktom podľa parentov
 *
 * @package Wame\ImportExport\EndCalls
 */
class DescendantProduct implements Call
{
    const TABLE_PRODUCT = 'wame_shop_product';


    public function __invoke(ImportExport &$importExport)
    {
//        \Tracy\Debugger::log('DescendantProduct');
        $logName = $importExport->getImportType() . " | EndCall DescendantProduct ";

        echo $logName . 'start';

        $repository = $importExport->getRepository(self::TABLE_PRODUCT);
        $importType = $importExport->getImportType();

        ini_set('memory_limit', '512M');
        Debugger::log('Descendant START', 'import');
//        $descendantProducts = $repository->getPairs(['descendant_product' => null, 'import_parent_id != ?' => '', 'import_type' => $importType], 'import_parent_id', 'import_parent_id');
        $descendantProducts = $repository->getPairs(['(descendant_product IS NULL OR descendant_product = ?)' => 0, 'import_type' => $importType], 'import_product_id', 'product_id');
        Debugger::log('Descendant get parents');
        $products = $repository->getPairs(['import_parent_id IN (?)' => array_keys($descendantProducts), 'import_type' => $importType], 'product_id', 'import_parent_id');
        Debugger::log('Descendant get childrens', 'import');

        $importIds = [];
        foreach ($products as $productId => $importParentId ) {
            $importIds[$importParentId][] = $productId;
            unset($products[$productId]);
        }
        $products = null;

        Debugger::log('Descendant pairing', 'import');
        foreach ($importIds as $importParentId => $productIds) {
            $repository->update(['product_id' => array_values($productIds), 'import_type' => $importType], ['descendant_product' =>$descendantProducts[$importParentId]]);
            unset($importIds[$importParentId]);
            unset($descendantProducts[$importParentId]);
        }
        Debugger::log('Descendant FINISH', 'import');

        $importIds = null;
        $descendantProducts = null;
    }

}
