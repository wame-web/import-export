<?php

namespace Wame\ImportExport\EndCalls;

use Tracy\Debugger;
use Wame\ImportExport\Calls\Call;
use Wame\ImportExport\ImportExport;


class ShopCategoryParent implements Call
{
    const TABLE = 'wame_shop_category';


    /** @var int */
    private $codeLength;

    /** @var string|null */
    private $parentColumn;


    public function __construct($codeLength = 0, $parentColumn = null)
    {
        $this->codeLength = $codeLength;
        $this->parentColumn = $parentColumn;
    }


    public function __invoke(ImportExport &$importExport)
    {
        $logName = $importExport->getImportType() . " | EndCall ShopCategoryParent ";

        echo $logName . 'start';
//        Debugger::log($logName . 'start');

        $repository = $importExport->getRepository(self::TABLE);
        $importType = $importExport->getImportType();
        $codeLength = $this->codeLength;

        $data = [];

        if ($codeLength == 0) {
            $categories = $repository->getObjectPairs(['import_type' => $importType], 'import_id');

            foreach ($categories as $importId => $category) {
                if ($category[$this->parentColumn] == '') continue;

                $data[$category[$this->parentColumn]][] = $category['category_id'];
            }

            foreach ($data as $parentCategoryId => $categoryIds) {
                $repository->update(['category_id IN (?)' => $categoryIds], ['parent_category_id' => $categories[$parentCategoryId]['category_id']]);
            }
        } else {
            $categories = $repository->getPairs(['import_type' => $importType], 'import_id', 'category_id');

            foreach ($categories as $importId => $categoryId) {
                $code = substr($importId, 0, -$codeLength);

                if ($code != '' && isset($categories[$code])) {
                    $data[$code][] = $categoryId;
                }
            }

            foreach ($data as $parentCategoryId => $categoryIds) {
                $repository->update(['category_id IN (?)' => $categoryIds], ['parent_category_id' => $categories[$parentCategoryId]]);
            }
        }

//        Debugger::log($logName . 'end');
    }

}
