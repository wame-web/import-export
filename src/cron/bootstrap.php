<?php

use Nette\Configurator;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../app/model/ConfigLoader.php';

$configurator = new Configurator;

//$configurator->setDebugMode(true);

$configurator->enableDebugger(__DIR__ . '/../log');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
    ->addDirectory(__DIR__)
    ->addDirectory(__DIR__ . '/../app')
    ->addDirectory(__DIR__ . '/../vendor/others')
    ->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$configLoader = new \App\Model\ConfigLoader();
$configLoader->addDirectory(__DIR__ . '/plugins/');
$configLoader->loadConfigs($configurator);

$container = $configurator->createContainer();

return $container;
