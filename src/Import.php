<?php

namespace Wame\ImportExport;

use App\Model\ImportExport\Configuration\FTPFastPlus\FtpDownloader;
use Nette\Utils\Validators;
use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Parser\UniqueNode;
use Prewk\XmlStringStreamer\Stream\Guzzle;
use Tracy\Debugger;

/**
 * Class Import
 *
 * @package Wame\ImportExport
 */
class Import extends ImportExport
{
    /** @var string */
    public $inputXmlPath;

	/** @var bool */
    public $deleteSource;

    /** @var array */
    protected $inputCriteria;

    /** @var array */
    public $columnSort = [];


    public function convertMulti($table, $converter)
    {
        $this->calls[] = function() use($table, $converter) {
            try {
                if (isset($this->output[$table])) {
                    foreach ($this->output[$table] as $key => $items) {
                        $this->output[$table][$key] = call_user_func_array($converter, [$items]);
                    }
                }
            } catch(\Exception $e) {
//                Debugger::log($e->getMessage());
            }
        };
    }


    public function copyColumnFromOtherTable($fromTable, $fromColumn, $toTable, $toColumn, $default = null, $removeIfMissed = false)
    {
        $this->calls[] = function() use ($fromTable, $fromColumn, $toTable, $toColumn, $default, $removeIfMissed)
        {
            try {
                if (isset($this->output[$toTable])) {
                    foreach ($this->output[$toTable] as $key => $item) {
                        if (isset($this->output[$fromTable][$key][$fromColumn])) {
                            $this->output[$toTable][$key][$toColumn] = $this->output[$fromTable][$key][$fromColumn];
                        } elseif ($removeIfMissed === true) {
                            unset($this->output[$toTable][$key]);
                        } else {
                            $this->output[$toTable][$key][$toColumn] = $default;
                        }
                    }
                }
            } catch(\Exception $e) {
//                Debugger::log($e->getMessage());
                Debugger::log($e);
            }
        };
    }


    public function mapMulti(string $xmlPath, array $map, array $defaults = [])
    {
        $this->calls[] = function() use (&$xmlPath, &$map, &$defaults) {
			//$i = 0;
            $this->output = [];
            $this->defaults = $defaults;

            if (isset($this->input[$xmlPath])) {
                foreach ($this->input[$xmlPath] as &$items) {
                    //if($i++ < $this->offset) continue;

                    foreach (array_keys($map) as $key) {
//                        if (strpos($key, '{') && strpos($key, '}')) {
//                            preg_match('/(\w+){(\w.+)}/', $key, $matches);
//
//                            dump($matches);
//                            dump($items);
//                            exit;
//                        } else
                        if (strpos($key, '->')) {
                            $v = $items;

                            foreach (explode('->', $key) as $mk) {
                                if (isset($v[$mk])) {
                                    $v = $v[$mk];
                                }
                            }

                            $this->mapMultiValue($map, $this->offset, $key, $v);
                        } elseif (isset($items[$key])) {
                            $this->mapMultiValue($map, $this->offset, $key, $items[$key]);
                        }
                    }

                    foreach ($items as $key => $item) {
                        if ($key == '@attributes') {
                            foreach ($item as $ak => &$ai) {
                                $key = "_$ak";
                                $this->mapMultiValue($map, $this->offset, $key, $ai);
                            }
                        }

                        if (array_key_exists($key, $map)) {
                            $table = $map[$key][0];

                            if (array_key_exists($table, $defaults)) {
                                foreach ($defaults[$table] as $k => &$d) {
                                    if (isset($this->output[$table][$this->offset][$k]) && $this->output[$table][$this->offset][$k]) continue;

                                    $this->output[$table][$this->offset][$k] = $d;
                                }
                            }
                        }
                    }

//                    foreach ($items as $key => $item) {
//                        if ($key == '@attributes') {
//                            foreach ($item as $ak => &$ai) {
//                                $key = "_$ak";
//                                $this->mapMultiValue($map, $this->offset, $key, $ai);
//                            }
//                        } else {
//                            $this->mapMultiValue($map, $this->offset, $key, $item);
//                        }
//
//                        if (array_key_exists($key, $map)) {
//                            $table = $map[$key][0];
//
//                            if (array_key_exists($table, $defaults)) {
//                                foreach ($defaults[$table] as $k => &$d) {
//                                    if (isset($this->output[$table][$this->offset][$k]) && $this->output[$table][$this->offset][$k]) continue;
//
//                                    $this->output[$table][$this->offset][$k] = $d;
//                                }
//                            }
//                        }
//                    }

                    $this->offset++;                //if($this->offset % $this->getChunk() == 0) break;
                }
            }
        };
    }


    private function mapMultiValue(&$map, $index, $key, $value)
    {
        if (array_key_exists($key, $map)) {
            $table = $map[$key][0];
            $column = $map[$key][1];
            $value = !empty($value) ? $value : null;
            $this->output[$table][$index][$column] = $value;
        }
    }


    public function mapTree(string $xmlPath, array $map, array $defaults = [])
    {
        $this->calls[] = function() use (&$xmlPath, &$map, &$defaults) {
            $this->output = [];
            $this->defaults = $defaults;

            if (isset($this->input[$xmlPath])) {
                foreach ($this->input[$xmlPath] as &$items) {
                    dump($items);
                    exit;

                }
            }
        };
    }


    /**
     * FTP file donwload
     *
     * @param string $host FTP host
     * @param string $user FTP username
     * @param string $pass FTP password
     * @param string $from FTP file path
     * @param string $to copy file path
     * @param string $fileName file name
     */
    public function ftpDownload($host, $user, $pass, $from, $to, $fileName)
    {
        $downloader = new FtpDownloader([
            'host' => $host,
            'user' => $user,
            'password' => $pass
        ]);

        if (!file_exists($to)) mkdir($to, 0777, true);

        $downloader->downloadFile($from, $to . $fileName);
        $downloader->close();
    }


	/**
	 * @param $file_source, absolute file path or url
	 * @param $file_target, absolute file path
	 * @return bool
	 */
	private function downloadAndSave($file_source, $file_target)
    {
		$rh = fopen($file_source, 'rb');
		$wh = fopen($file_target, 'wb');
		if ($rh === false || $wh === false) return false;
		while (!feof($rh)) {
			if (fwrite($wh, fread($rh, 1024)) === false) return false;
		}
		fclose($rh);
		fclose($wh);
		return true;
	}


	/**
	 * @param $file_source, absolute file path or url
	 * @return bool
	 */
	private function deleteSaved($file_source)
    {
		return unlink($file_source);
	}


	/**
	 * Change encoding for file
	 *
	 * @param $filePath
	 * @param $fromEncode
	 * @param $toEncode
	 */
	public function changeEncoding($filePath, $fromEncode, $toEncode)
	{
		$xml_string = file_get_contents($filePath);

		$xml_string = @iconv($fromEncode, $toEncode, $xml_string);

        $xml_string = str_replace('encoding="' . $fromEncode .'"', 'encoding="' . $toEncode . '"', $xml_string);

		file_put_contents($filePath,$xml_string);
	}



    /**
     * Set input XML source
     *
     * @param string $source source
     * @param string $savedFilePath relative path to save
     * @param string $nodeName node name
     */
    public function setInputXml($source, $savedFilePath = null, $nodeName = 'product', $depth = null)
    {
        $logName = $this->getImportType() . ' | Import';

//        Debugger::log($logName . " - " . $source . "\n");
        echo $logName. " - " . $source . "\n";

        $importExportSettingsRepository = $this->container->getByType('App\Model\ImportExport\ImportExportSettingsRepository');
		$this->inputXmlPath = NULL;

		if (is_string($source)) {
			if (Validators::isUrl($source)) {
				if ($savedFilePath == NULL) {
					$this->inputXmlPath = $source;
				} else {
					$fullFilePathSaved = $this->container->parameters['appDir'] . "/" . $savedFilePath;
					if (!file_exists($fullFilePathSaved)) {                    //$content = file_get_contents($source);file_put_contents($fullFilePathSaved, $content);
						if ($this->downloadAndSave($source, $fullFilePathSaved)) {
							if (file_exists($fullFilePathSaved)) {
								$this->inputXmlPath = $fullFilePathSaved;
								$this->deleteSource = true;
							}
						}
					} else {
						$this->inputXmlPath = $fullFilePathSaved;
						$this->deleteSource = true;
					}
				}
			}
			if (is_file($source)) {
				if (file_exists($source)) {
					$this->inputXmlPath = $source;
					$this->deleteSource = true;
				}
			} else {
				$fullFilePathSaved = $this->container->parameters['appDir'] . "/" . $source;
				if (file_exists($fullFilePathSaved)) {
					$this->inputXmlPath = $fullFilePathSaved;
					$this->deleteSource = true;
				}
			}



		}

        $this->output = [];
        $this->input = [];

//        $stream = new Guzzle($this->inputXmlPath);
//
//        $parser = new UniqueNode(["uniqueNode" => $nodeName]);
//        $streamer = new XmlStringStreamer($parser, $stream);
//
//        $i = 0;
//        while ($node = $streamer->getNode()) {
//            if ($i++ < $this->offset) continue;
//
//            echo "Converting node " . $i . "\n";
//
//            if ($i % $this->getChunk() == 0) {
//                $importExportSettingsRepository->update(['name' => 'import' . $this->class->getName() . '_progress'], ['value' => $i]);
//            }
//
//            $item = json_decode(json_encode(simplexml_load_string($node, "SimpleXMLElement", LIBXML_NOCDATA)), true);
//            $this->input[$nodeName][] = $item;
//
//            if ($i >= $this->offset + $this->getChunk()) break;
//        }
//
//        if (empty($this->input[$nodeName])) {
//            $importExportSettingsRepository->update(['name' => 'import' . $this->class->getName() . '_progress'], ['value' => 0]);
//            $this->finished = true;
//
//            if ($this->deleteSource === true) {
//                if ($this->inputXmlPath != null && is_file($this->inputXmlPath)) {
//                    $stream = null;
//                    $streamer = null;
//                    $this->deleteSaved($this->inputXmlPath);
//                    $this->inputXmlPath = NULL;
//                }
//            }
//        }
//
//        dump($this->input);

		$this->calls[] = function() use ($importExportSettingsRepository, $nodeName, $depth, $logName) {
            $settings = $importExportSettingsRepository->findBy(['name' => 'import' . $this->class->getName() . '_progress'])->fetch();
            $this->offset = $settings->value;

            $this->output = [];
            $this->input = [];

            echo $logName . "START call\n";

            if (($this->finished === true) && ($this->deleteSource === true)) {
				if (($this->inputXmlPath != null) && is_file($this->inputXmlPath)) {
//					$this->deleteSaved($this->inputXmlPath);
					$this->inputXmlPath = NULL;
				}
			}

            if (is_string($this->inputXmlPath)) {				//$this->input = json_decode(json_encode(simplexml_load_string(file_get_contents($this->inputXmlPath),"SimpleXMLElement",LIBXML_NOCDATA)),true);
				try {
					$stream = new Guzzle($this->inputXmlPath);

					if ($depth) {
                        $parser = new XmlStringStreamer\Parser\StringWalker(["captureDepth" => $depth, "extractContainer" => true]);
                    } else {
					    $parser = new UniqueNode(["uniqueNode" => $nodeName, 'checkShortClosing' => true]);
                    }

					$streamer = new XmlStringStreamer($parser, $stream);

					$i = 0;

                    echo $logName . "START while\n";
					while ($node = $streamer->getNode()) {
						if ($i++ < $this->offset) continue;

						echo $logName . "Converting node " . $i . "\n";

						if ($i % $this->getChunk() == 0) {
                            echo $logName . "UPDATE import" . $this->class->getName() . "_progress - " . $i . " \n";
							$importExportSettingsRepository->update(['name' => 'import' . $this->class->getName() . '_progress'], ['value' => $i]);
						}

						if (mb_detect_encoding($node, 'UTF-8', true) === false) {
                            $node = utf8_decode($node);
                        }

						$item = json_decode(json_encode(simplexml_load_string($node, "SimpleXMLElement", LIBXML_NOCDATA)), true);

						if ($item) $this->input[$nodeName][] = $item;

						if ($i >= $this->offset + $this->getChunk()) {
                            echo $logName . 'COUNT - ' . count($this->input[$nodeName]) . " break; \n";
                            break;
                        }

//						echo $i . ' | offset: ' . $this->offset . ' | chunk: ' . $this->getChunk() . ' | spolu: ' . ($this->offset + $this->getChunk()) . "\n";
					}

                    echo $logName . "END while\n";

					$finished = false;

                    if (!isset($this->input[$nodeName])) {
                        $finished = true;
                    } elseif (empty($this->input[$nodeName])) {
					    $finished = true;
                    } elseif (count($this->input[$nodeName]) < $this->getChunk()) {
                        $finished = true;
                    }

					if ($finished == true) {
                        echo $logName . "EMPTY\n";
//                        \Tracy\Debugger::log($this->class->getName());
						$importExportSettingsRepository->update(['name' => 'import' . $this->class->getName() . '_progress'], ['value' => 0]);
						$this->finished = true;

						if ($this->deleteSource == true) {
							if ($this->inputXmlPath != null && is_file($this->inputXmlPath)) {
								$stream = null;
								$streamer = null;
//								$this->deleteSaved($this->inputXmlPath);
								$this->inputXmlPath = NULL;
							}
						}
					}
				} catch (\Exception $exception) {
//					\Tracy\Debugger::log($exception->getMessage());
					\Tracy\Debugger::log($exception);
				}
            }

        };
    }


    /** {@inheritdoc} */
    public function setInput(array $input = []): ImportExport
    {
        if ($this->inputRepository) {
            $select = $this->select ? implode(',', $this->select) : implode(',', array_keys($this->map));
            // TODO: move criteria to parameters
            $items = $this->inputRepository->select($select)->where('Modify_Date > ?', $this->settings['date'])->fetchAll();
            $input = [];
            foreach ($items as $r) $input[] = $r->toArray(); // on ActiveRow
            $this->input = $input;
        }

        return $this;
    }


    /**
     * Set input criteria
     *
     * @param array $criteria criteria
     */
    public function setInputCriteria(array $criteria)
    {
        $this->inputCriteria = $criteria;
    }


    /**
     * Set column sort for multiple insert or update
     *
     * @param array $list
     *
     * @return $this
     */
    public function setColumnSort(array $list)
    {
        $this->columnSort = $list;

        return $this;
    }

}
/*
 * TODO:
 * - merge setInputXml to setInput and make it universal to read from database, xml, csv, json, ...
 * - merge convertMulti to convert
 * - merge mapMulti to map
 * - move convertMulti to ImportExport
 * - move mapMulti to ImportExport
 */
