<?php

namespace Wame\ImportExport;

use Nette\DI\Container;

class Executor
{
    /** @var Container */
    private $context;


    function __construct(Container $context)
    {
        $this->context = $context;
    }


    /**
     * @param $type
     * @param $task
     */
    public function import($type, $task = null)
    {
        $types = $this->getImportTypes($type);

        foreach($types as $t) {
            $t->import($task);
        }
    }
    /**
     * @param $type
     * @param $task
     */
    public function export($type, $task = null)
    {
        $types = $this->getExportTypes($type);

        foreach($types as $t) {
            $t->export($task);
        }
    }

    /**
     * @param $type
     * @return IImportConfiguration|IImportConfiguration[]
     */
    public function getImportTypes($type)
    {
        return $this->loadTypes(IImportConfiguration::class, $type);
    }

    /**
     * @param $type
     * @return IExportConfiguration|IExportConfiguration[]
     */
    public function getExportTypes($type)
    {
        return $this->loadTypes(IExportConfiguration::class, $type);
    }


    /**
     * @param IDataConfiguration $class
     * @param null $name
     * @return IDataConfiguration|IDataConfiguration[]
     */
    private function loadTypes($class, $name = null)
    {
        $types = [];

        foreach($this->context->findByType($class) as $typeName) {
            $type = $this->context->getService($typeName);

            if($name && $type->getName() == $name) {
                return [$type];
            }

            $types[] = $type;

            // TODO: doesnt work
//            yield $type;
        }

        return $types;
    }

}