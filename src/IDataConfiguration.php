<?php

namespace Wame\ImportExport;

interface IDataConfiguration
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getIdentifierName();

    /**
     * @return Form|null
     */
    public function getSettingsEditor();

    /**
     * @return string
     */
    public function getSettingsEditorName();

    /**
     * @return boolean
     */
    public function isImport();

    /**
     * @return boolean
     */
    public function isExport();

    /**
     * @return boolean
     */
    public function isRunnedByConsole();

}