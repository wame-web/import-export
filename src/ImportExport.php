<?php

namespace Wame\ImportExport;

use Nette\Database\Context;
use Nette\Database\Table\Selection;
use Nette\DI\Container;
use Port\Reader\ArrayReader;
use Port\Steps\Step;
use Port\Steps\Step\ConverterStep;
use Port\Steps\Step\MappingStep;
use Port\Steps\StepAggregator;
use Port\Writer\ArrayWriter;
use Tracy\Debugger;


/**
 * Class ImportExport
 *
 * @package Wame\ImportExport
 */
abstract class ImportExport
{
    /** @var int */
    public $size;
    /** @var int */
    public $offset = 0;
    /** @var array */
    public $input = [];
    /** @var array */
    public $output = [];
    /** @var array */
    public $report;
    /** @var array */
    public $settings;
    /** @var array */
    public $data = [];
    /** @var array */
    public $defaults = [];
    /** @var int */
    protected $chunk = 100;
    /** @var Step[] */
    protected $steps = [];
    /** @var array */
    protected $calls = [];
    /** @var array */
    protected $endCalls = [];
    /** @var mixed */
    protected $inputRepository;
    /** @var mixed */
    protected $outputRepository;
    /** @var Container */
    public $container;
    /** @var BaseRepository */
    protected $importExportSettingsRepository;
    /** @var array */
    protected $map = [];
    /** @var array */
    protected $select = [];
    /** @var string */
    protected $inputTableName;
    /** @var string */
    protected $outputTableName;
    /** @var Context */
    protected $inputDatabase;
    /** @var Context */
    protected $outputDatabase;
    /** @var array */
    protected $repositories;
    /** @var bool */
    public $finished = false;
    /** @var string */
    private $importType;

    /** @var object */
    protected $class;


    /**
     * ImportExport constructor.
     *
     * @param Container $container
     * @param Context $database
     */
    function __construct(Container $container, Context $database)
    {
        $this->container = $container;
        $this->inputDatabase = $database;
    }


    public function setImportExportSettingsRepository(BaseRepository $importExportSettingsRepository, string $type, array $vals = [])
    {
        $this->importExportSettingsRepository = $importExportSettingsRepository;
        $this->settings = $importExportSettingsRepository->getSettings($type, $vals);
    }


    /* methods ****************************************************************/

    /**
     * Call function
     *
     * @param callable $function function
     * @return ImportExport
     */
    public function call(callable $function): ImportExport
    {
        $this->calls[] = $function;

        return $this;
    }


    /**
     * Call function after end import
     *
     * @param callable $function function
     * @return ImportExport
     */
    public function endCall(callable $function): ImportExport
    {
        $this->endCalls[] = $function;

        return $this;
    }


    /**
     * Convert
     *
     * @param array|callable $converter converter
     * @return ImportExport
     */
    public function convert($converter): ImportExport
    {
        $converterStep = new ConverterStep();

        if (is_array($converter)) {
            $converter = function ($input) use ($converter) {
                return $input + $converter;
            };
        }

        if (is_callable($converter)) {
            $converterStep->add($converter);
        }

        $this->steps[] = $converterStep;

        return $this;
    }


    /**
     * Execute workflow
     *
     * @return ImportExport
     * @throws \Exception
     */
    public function execute(): ImportExport
    {
//        Debugger::log('-- import start --');
//        Debugger::log('memory: ' . memory_get_usage());
//        Debugger::log('real memory: ' . memory_get_usage(true));

        $iterator = 0;
        $this->offset = 0;
        $this->steps = [];
        $this->input = [];
        $this->output = [];

        try {
            $this->setInput();

            if (!empty($this->steps)) {
                $reader = new ArrayReader($this->input);
                $writer = new ArrayWriter($this->output);
                $workflow = new StepAggregator($reader);
                $workflow->addWriter($writer);

                foreach ($this->steps as $step) {
                    $workflow->addStep($step);
                }

                $workflow->process();
            }

            while (!$this->finished) {
//                Debugger::log('cycle: ' . $iterator++);

                foreach ($this->calls as $call) {
//                    if ($this->finished) break;

                    set_time_limit(600);
//                    Debugger::log(get_class($call));
//                    Debugger::log('memory: ' . memory_get_usage());
//                    Debugger::log('real memory: ' . memory_get_usage(true));
                    call_user_func_array($call, [&$this]);
                }

//                Debugger::log('END calls');
            }
        } catch (\Exception $e) {
            Debugger::log($e);
            throw $e;
        } finally {
//            Debugger::log('END calls');
        }

        try {
            foreach ($this->endCalls as $call) {
                set_time_limit(600);
//                Debugger::log(get_class($call));
//                Debugger::log('memory: ' . memory_get_usage());
//                Debugger::log('real memory: ' . memory_get_usage(true));
                call_user_func_array($call, [&$this]);
            }
        } catch (\Exception $e) {
            Debugger::log($e);
            throw $e;
        } finally {
//            Debugger::log('-- import finish --');
        }

        return $this;
    }


    /**
     * Select
     *
     * @param array $columns columns
     * @return ImportExport
     */
    public function select(array $columns): ImportExport
    {
        $this->select = $columns;
        return $this;
    }


    /**
     * Map
     *
     * @param array $map map
     * @return ImportExport
     */
    public function map(array $map): ImportExport
    {
        $this->map = array_replace($this->map, $map);
        $mappingStep = new MappingStep();
        foreach($map as $k => &$v) {
            if(!$v) continue;
            $mappingStep->map("[$k]", "[$v]");
        }
        $this->steps[] = $mappingStep;
        return $this;
    }


    /**
     * Set input
     *
     * @param array $input input
     * @return ImportExport
     */
    abstract public function setInput(array $input = []): ImportExport;


    /* getters and setters ****************************************************/

    /**
     * Get input
     *
     * @return array
     */
    public function getInput(): array
    {
        return $this->input;
    }


    /**
     * Get output
     *
     * @return array
     */
    public function getOutput(): array
    {
        return $this->output;
    }


    /**
     * Set output
     *
     * @param array $output
     * @return ImportExport
     */
    public function setOutput(array $output): ImportExport
    {
        if (count($output) == 1 && isset($output[0]) && substr($output[0], 0, 1) == '&') {
            $keys = explode('.', substr($output[0], 1));

            $output['shop_order'] = $this->data['shop_order'];
        }

        $this->output = $output;

        return $this;
    }


    /**
     * Get chunk
     *
     * @return int
     */
    public function getChunk(): int
    {
        return $this->chunk;
    }


    /**
     * Set chunk size
     *
     * @param int $chunk
     * @return ImportExport
     */
    public function chunk(int $chunk): ImportExport
    {
        $this->chunk = $chunk;
        return $this;
    }


    /**
     * Get steps
     *
     * @return Step[]
     */
    public function getSteps(): array
    {
        return $this->steps;
    }


    /**
     * Set steps
     *
     * @param Step[] $steps
     * @return ImportExport
     */
    public function setSteps(array $steps): ImportExport
    {
        $this->steps = $steps;
        return $this;
    }


    /**
     * Get calls
     *
     * @return array
     */
    public function getCalls(): array
    {
        return $this->calls;
    }


    /**
     * Set calls
     *
     * @param array $calls
     * @return ImportExport
     */
    public function setCalls(array $calls): ImportExport
    {
        $this->calls = $calls;
        return $this;
    }


    /**
     * Set repository
     *
     * @param string $tableName table name
     * @param mixed $repository repository
     * @return $this
     */
    public function setRepository(string $tableName, $repository)
    {
        $this->repositories[$tableName] = $repository;
        return $this;
    }


    /**
     * Get repository
     *
     * @param string $tableName table name
     * @return mixed
     */
    public function getRepository(string $tableName)
    {
        return $this->repositories[$tableName];
    }


    /**
     * Get input repository
     *
     * @return BaseRepository|Selection
     */
    public function getInputRepository()
    {
        return $this->inputRepository;
    }


    /**
     * Set input repository
     *
     * @param BaseRepository|string $source source
     * @param Context|null $db database context
     * @param array $criteria criteria
     * @return ImportExport
     */
    public function setInputRepository($source, Context $db = null, array $criteria = []): ImportExport
    {
        if ($source instanceof BaseRepository) {
            $this->inputRepository = $source;
        } else if(is_string($source)) {
            $db = $db ?? $this->container->getService('database.loader.factory');
            $this->inputRepository = $db->table($source)->where($criteria);
            $this->inputDatabase = $db;
            $this->inputTableName = $source;
        }

        return $this;
    }


    /**
     * Get output repository
     *
     * @return BaseRepository|Selection
     */
    public function getOutputRepository()
    {
        return $this->outputRepository;
    }


    /**
     * Set output repository
     *
     * @param BaseRepository|string $source source
     * @param Context|null $db database context
     * @param array $criteria criteria
     * @return ImportExport
     */
    public function setOutputRepository($source, Context $db = null, array $criteria = []): ImportExport
    {
        if  ($source instanceof BaseRepository) {
            $this->outputRepository = $source;
        } else if(is_string($source)) {
            $db = $db ?? $this->container->getService('database.loader.factory');
            $this->outputRepository = $db->table($source)->where($criteria);
            $this->outputDatabase = $db;
            $this->outputTableName = $source;
        }

        return $this;
    }


    /**
     * Get input table name
     *
     * @return string
     */
    public function getInputTableName(): string
    {
        return $this->inputTableName;
    }


    /**
     * Get output table name
     *
     * @return string
     */
    public function getOutputTableName(): string
    {
        return $this->outputTableName;
    }


    /**
     * Get input database
     *
     * @return Context
     */
    public function getInputDatabase(): Context
    {
        return $this->inputDatabase;
    }


    /**
     * Get output database
     *
     * @return Context
     */
    public function getOutputDatabase(): Context
    {
        return $this->outputDatabase;
    }


    /**
     * Get import type
     *
     * @return string
     */
    public function getImportType(): string
    {
        return $this->importType;
    }


    /**
     * Set import type
     *
     * @param string $importType import type
     * @return ImportExport
     */
    public function setImportType(string $importType): ImportExport
    {
        $this->importType = $importType;

        return $this;
    }


    /**
     * Set class
     *
     * @param $service
     *
     * @return $this
     */
    public function setClass($service)
    {
        $this->class = $service;

        return $this;
    }


    public function getClass()
    {
        return $this->class;
    }

}
