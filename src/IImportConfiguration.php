<?php

namespace Wame\ImportExport;

use Nette\Utils\Strings;

abstract class  IImportConfiguration implements IDataConfiguration
{

    public abstract function import();

    /**
     * @return Form|null
     */
    public function getSettingsEditor()
    {

    }

    /**
     * @return string
     */
    public function getIdentifierName()
    {
        return 'import' . Strings::capitalize(str_replace('-', '', Strings::webalize($this->getName())));
    }

    /**
     * @return string
     */
    public function getSettingsEditorName()
    {
        return $this->getIdentifierName() . 'settings';
    }

    public function isImport()
    {
        return true;
    }

    public function isExport()
    {
        return false;
    }

}