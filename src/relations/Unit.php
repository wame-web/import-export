<?php

namespace Wame\ImportExport\Relations;

use App\Utils\Numbers;
use Tracy\Debugger;
use Wame\ImportExport\ImportExport;
use Wame\ImportExport\Relations\Relation;


class Unit extends Relation
{
    /** @var string */
    private $tableName;

    /** @var string */
    private $column;

    /** @var string */
    private $key;

    /** @var array */
    private $unitList;


    public function __construct($tableName, $column, $key)
    {
        $this->tableName = $tableName; // wame_shop_product_price
        $this->column = $column; // unit_id
        $this->key = $key; // unit
    }


    function __invoke(ImportExport &$importExport)
    {
//        \Tracy\Debugger::log('Unit');
        $logName = $importExport->getImportType() . " | Unit relation ";

        echo $logName . "\n";
//        Debugger::log($logName . "__invoke start");

        $this->unitList = $importExport->getRepository('wame_unit')->getPairs([], $this->key, 'id');
//        Debugger::log($logName . "__invoke after get unitList");

        foreach ($importExport->output[$this->tableName] as $i => $item) {
//            Debugger::log($logName . "__invoke foreach - " . $i);

            $importExport->output[$this->tableName][$i][$this->column] = $this->getUnitId($item);
        }

//        Debugger::log($logName . "__invoke end");
    }


    private function getUnitId($item)
    {
        if (isset($item[$this->column]) && isset($this->unitList[$item[$this->column]])) {
            return $this->unitList[$item[$this->column]];
        }

        return null;
    }

}
