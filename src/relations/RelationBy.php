<?php

namespace Wame\ImportExport\Relations;

use Tracy\Debugger;
use Wame\ImportExport\ImportExport;
use Wame\ImportExport\Relations\Relation;


class RelationBy extends Relation
{
    /** @var string */
    private $fromTable;

    /** @var string */
    private $from;

    /** @var string */
    private $toTable;

    /** @var string */
    private $key;

    /** @var string */
    private $to;


    public function __construct($fromTable, $from, $toTable, $key, $to)
    {
        $this->fromTable = $fromTable;
        $this->from = $from;
        $this->toTable = $toTable;
        $this->key = $key;
        $this->to = $to;
    }


    function __invoke(ImportExport &$importExport)
    {
        $logName = $importExport->getImportType() . " | RelationBy " . $this->from . ' -> ' . $this->toTable . '.' . $this->to;

        if (!isset($importExport->output[$this->fromTable])) return;

        $items = $importExport->output[$this->fromTable];

        $list = $importExport->getRepository($this->toTable)->getPairs([$this->key => array_column($items, $this->from)], $this->key, $this->to);

//        Debugger::log($logName . "__invoke after get list");

        foreach ($items as $i => $item) {
//            if ($item['import_id'] == '24971') {
//                dump('RelationBy');
//                dump($items);
//                dump($item);
//            }
//            Debugger::log($logName . "__invoke foreach - " . $i);

            if (isset($list[$item[$this->from]])) {
                $importExport->output[$this->fromTable][$i][$this->from] = $list[$item[$this->from]];
            } else {
                unset($importExport->output[$this->fromTable][$i]);
            }
        }

//        dump('RelationBy ALL');
//        dump($importExport->output[$this->fromTable]);

//        Debugger::log($logName . "__invoke end");
    }

}
