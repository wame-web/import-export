<?php

namespace Wame\ImportExport\Relations;

use Tracy\Debugger;
use Wame\ImportExport\ImportExport;
use Wame\ImportExport\Relations\Relation;


class ShopProductInCategory extends Relation
{
    const TABLE_SHOP_PRODUCT_IN_CATEGORY = 'wame_shop_product_in_category';
    const TABLE_SHOP_PRODUCT = 'wame_shop_product';
    const TABLE_SHOP_CATEGORY = 'wame_shop_category';


    function __invoke(ImportExport &$importExport)
    {
        $logName = $importExport->getImportType() . " | ShopProductInCategory relation ";

        echo $logName . "\n";
//        Debugger::log($logName . "__invoke start");

        $inOutput = array_column($importExport->output[self::TABLE_SHOP_PRODUCT], 'import_product_id');
        $products = $importExport->getRepository(self::TABLE_SHOP_PRODUCT)->getPairs(['import_type' => $importExport->getImportType(), 'import_product_id IN (?)' => $inOutput], 'import_product_id', 'product_id');
//        Debugger::log($logName . "__invoke after get products");

        $categories = $importExport->getRepository(self::TABLE_SHOP_CATEGORY)->getPairs([], 'import_id', 'category_id');
//        Debugger::log($logName . "__invoke after get categories");

//        Debugger::log($logName . "__invoke before foreach");

        if (!isset($importExport->output[self::TABLE_SHOP_CATEGORY])) return;

        foreach($importExport->output[self::TABLE_SHOP_PRODUCT] as $i => $item) {
//            Debugger::log($logName . "__invoke foreach - " . $i);

            if (!isset($importExport->output[self::TABLE_SHOP_PRODUCT][$i])) continue;
            if (!isset($products[$importExport->output[self::TABLE_SHOP_PRODUCT][$i]['import_product_id']])) continue;

            if (!isset($importExport->output[self::TABLE_SHOP_CATEGORY][$i])) continue;
            if (!isset($categories[$importExport->output[self::TABLE_SHOP_CATEGORY][$i]['import_id']])) continue;

            $importExport->output[self::TABLE_SHOP_PRODUCT_IN_CATEGORY][] = [
                'product_id' => $products[$importExport->output[self::TABLE_SHOP_PRODUCT][$i]['import_product_id']],
                'category_id' => $categories[$importExport->output[self::TABLE_SHOP_CATEGORY][$i]['import_id']]
            ];
        }

//        Debugger::log($logName . "__invoke after foreach");
//        Debugger::log($logName . "__invoke end");
    }

}
