<?php

namespace Wame\ImportExport\Relations;

use App\Utils\Numbers;
use Tracy\Debugger;
use Wame\ImportExport\ImportExport;
use Wame\ImportExport\Relations\Relation;


class ShopProductPriceOld extends Relation
{
    const TABLE_SHOP_PRODUCT = 'wame_shop_product';
    const TABLE_SHOP_PRODUCT_PRICE = 'wame_shop_product_price';
    const TABLE_SHOP_SETTINGS = 'wame_shop_settings';


    /** @var array */
    protected $shopSettings;

    /** @var array */
    protected $products;


    function __invoke(ImportExport &$importExport)
    {

//        \Tracy\Debugger::log('ShopProductPrice');
        $logName = $importExport->getImportType() . " | ShopProductPrice relation ";

        echo $logName . "\n";

        $marginName = $importExport->getImportType() . 'Margin';

//        Debugger::log($logName . "__invoke start");
        $this->shopSettings = $importExport->getRepository(self::TABLE_SHOP_SETTINGS)->getPairs(['name IN (?)' => [$marginName, 'defaultTax'], 'lang' => 'sk'], 'name', 'value');
//        Debugger::log($logName . "__invoke after get settings");

        $inOutput = array_column($importExport->output[self::TABLE_SHOP_PRODUCT], 'import_product_id');
        $this->products = $importExport->getRepository(self::TABLE_SHOP_PRODUCT)->getPairs(['import_type' => $importExport->getImportType(), 'import_product_id IN (?)' => $inOutput], 'import_product_id', 'product_id');
//        Debugger::log($logName . "__invoke after get products");

        $prices = $importExport->getRepository(self::TABLE_SHOP_PRODUCT_PRICE)->getTable()
            ->select('id, shop_product_id, purchase_price, margin, prices_group, price, price_with_tax, discount, discount_type')
            ->where(['status' => 1, 'import_type' => $importExport->getImportType()])->fetchPairs('shop_product_id');
//        Debugger::log($logName . "__invoke after get prices");

//        $importExport->output[self::TABLE_SHOP_PRODUCT_PRICE . '_status'] = [];
//        Debugger::log($logName . "__invoke before foreach");

        foreach ($importExport->output[self::TABLE_SHOP_PRODUCT_PRICE] as $i => $item) {
//            Debugger::log($logName . "__invoke foreach - " . $i);



            $productPrice = $importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i];

            // Ak ma produkt v XML cenu sdph5 a bezdph5, nastavi sa mu táto cena
            $inputI = array_search($importExport->output[self::TABLE_SHOP_PRODUCT][$i]['import_product_id'] ,array_column($importExport->input['produkt'], 'plu'));
            $productXmlPrice = $importExport->input['produkt'][$inputI];
            $importProductId = $importExport->output[self::TABLE_SHOP_PRODUCT][$i]['import_product_id'];
            $shopProductId = isset($this->products[$importProductId]) ? $this->products[$importProductId] : null;

            $declineSpecialPrice = 0;
            if (isset($productXmlPrice['predajneceny']['bezdph5']) && isset($productXmlPrice['predajneceny']['sdph5'])) {
                $beforeDiscountPrice = $productPrice['purchase_price'];
                $beforeDiscountPriceWithTax = $productPrice['purchase_price_with_tax'];
                $productPrice['purchase_price'] = $productXmlPrice['predajneceny']['bezdph5'];
                $productPrice['purchase_price_with_tax'] = $productXmlPrice['predajneceny']['sdph5'];
                $declineSpecialPrice = 1;
            }

            $importId = $importExport->output[self::TABLE_SHOP_PRODUCT][$i]['import_product_id'];
            $tax = $this->shopSettings['defaultTax'];
            $margin = isset($this->shopSettings[$marginName]) ? $this->shopSettings[$marginName] : 0;

            $purchasePrice = $productPrice['purchase_price'];
            $purchasePriceWithTax = $productPrice['purchase_price_with_tax'];

            $price = $productPrice['purchase_price'];
            $priceWithTax = $productPrice['purchase_price_with_tax'];

            // Vyratanie zlavy od aktualnej sumy v DB odráta importovanú sumu
//            if (isset($item['discount_factor'])) {
//                $discount = 0;
//                $discountType = 1;
//
//                if ($item['discount_factor'] == 'T' && isset($prices[$shopProductId])) {
//                    $discount = $priceWithTax - $prices[$shopProductId]['price_with_tax'];
//                    $discountType = 2;
//                }
//
//                $importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i]['discount'] = $discount;
//                $importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i]['discount_type'] = $discountType;
//
//                unset($importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i]['discount_factor']);
//            }

            if ($purchasePrice === null || is_array($purchasePrice)) {
                unset($importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i]);
                continue;
            } else {

                //purchasePriceWithTax = Numbers::plusPercent($purchasePrice, $tax);
                //$price = Numbers::plusPercent($purchasePrice, $margin);
                //$priceWithTax = Numbers::plusPercent($price, $tax);

                if (isset($prices[$shopProductId]) && $purchasePrice != $prices[$shopProductId]['purchase_price']) {
                    $margin = $prices[$shopProductId]['margin'];
                }

                if ($declineSpecialPrice == 1) {
                    $importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i] += [
                        'shop_product_id' => $shopProductId,
                        'import_id' => $importId,
                        'margin' => $margin,
                        'tax' => $tax,
                        'purchase_price_with_tax' => $purchasePriceWithTax,
                        'price' => $price,
                        'price_with_tax' => $priceWithTax,
                        'before_discount_price' => $beforeDiscountPrice,
                        'before_discount_price_with_tax' => $beforeDiscountPriceWithTax,
                        'discount' => ($beforeDiscountPriceWithTax-$priceWithTax),
                        'discount_type' => 2,
                        'create_date' => date('Y-m-d H:i:s')
                    ];
                } else {
                    $importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i] += [
                        'shop_product_id' => $shopProductId,
                        'import_id' => $importId,
                        'margin' => $margin,
                        'tax' => $tax,
                        'purchase_price_with_tax' => $purchasePriceWithTax,
                        'price' => $price,
                        'price_with_tax' => $priceWithTax,
                        'before_discount_price' => 0,
                        'before_discount_price_with_tax' => 0,
                        'discount' => 0,
                        'discount_type' => 2,
                        'create_date' => date('Y-m-d H:i:s')
                    ];
                }

                // Ked mal produkt nastavenu zlavu tak ju preratame
                /*                if (isset($prices[$shopProductId]) && $prices[$shopProductId]['discount'] > 0) {
                                    $output = $prices[$shopProductId];

                                    $importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i] += [
                                        'price' => $output['discount_type'] ? ($output['price_with_tax'] / (($output['discount'] / 100) + 1)) / (($tax / 100) + 1) : ($output['price_with_tax'] - $output['discount']) / (($output['tax'] / 100) + 1),
                                        'price_with_tax' => $output['discount_type'] ? $output['price_with_tax'] / (($output['discount'] / 100) + 1) : $output['price_with_tax'] - $output['discount'],
                                        'before_discount_price' => $output['price'],
                                        'before_discount_price_with_tax' => $output['price_with_tax']
                                    ];

                                    unset($prices[$shopProductId]);
                                }*/
            }
        }
//        Debugger::log($logName . "__invoke end");

        unset($inOutput);
        $this->products = null;
        $prices = null;
    }

}
