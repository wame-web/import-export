<?php

namespace Wame\ImportExport\Relations;

use Tracy\Debugger;
use Wame\ImportExport\ImportExport;
use Wame\ImportExport\Relations\Relation;


class ShopManufacturer extends Relation
{
    const TABLE_PRODUCT = 'wame_shop_product';
	const TABLE_SHOP_PRODUCT_PRICE = 'wame_shop_product_price';
	const COLUMN_NAME = 'shop_manufacturer_id';


    /** @var array */
    private $manufacturerList;


    function __invoke(ImportExport &$importExport)
    {
//        \Tracy\Debugger::log('ShopManufacturer');
        $logName = $importExport->getImportType() . " | ShopManufacturer relation ";

        $this->manufacturerList = $importExport->getRepository('wame_shop_manufacturer')->getPairs(['import_type' => $importExport->getImportType()], 'import_id', 'id');
//        Debugger::log($logName . "__invoke after get manufacturerList");

        foreach ($importExport->output[self::TABLE_PRODUCT] as $i => $item) {
//            Debugger::log($logName . "__invoke foreach - " . $i);

// Odstraňovalo produkty z output (z 1000 odstránilo 12)
//			if(is_array($importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i]['purchase_price'])) {
//				unset($importExport->output[self::TABLE_PRODUCT][$i]);
//				unset($importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i]);
//				continue;
//			}


            $importExport->output[self::TABLE_PRODUCT][$i][self::COLUMN_NAME] = $this->getManufacturerId($item);
        }

//        Debugger::log($logName . "__invoke end");
    }


    private function getManufacturerId(array $input)
    {
        if (isset($input[self::COLUMN_NAME]) && isset($this->manufacturerList[$input[self::COLUMN_NAME]])) {
            return $this->manufacturerList[$input[self::COLUMN_NAME]];
        }

        return null;
    }

}
