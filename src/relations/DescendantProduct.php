<?php

namespace Wame\ImportExport\Relations;

use Tracy\Debugger;
use Wame\ImportExport\ImportExport;
use Wame\ImportExport\Relations\Relation;


class DescendantProduct extends Relation
{
    const TABLE_PRODUCT = 'wame_shop_product';
    const COLUMN_NAME = 'descendant_product';


    function __invoke(ImportExport &$importExport)
    {
        $logName = $importExport->getImportType() . " | DescendantProduct relation ";

        $descendantProducts = [];

        foreach ($importExport->output[self::TABLE_PRODUCT] as $i => $item) {
            if ($item[self::COLUMN_NAME] === 0) continue;
            if (!$item['import_parent_id']) continue;

            $descendantProducts[$i] = $item['import_parent_id'];
        }

//        Debugger::log($logName . ' - find ' . count($descendantProducts) . ' descendantProducts');

        if (count($descendantProducts) > 0) {
//            Debugger::log($logName . ' - process');

            $products = $importExport->getRepository(self::TABLE_PRODUCT)->getPairs(['descendant_product' => 0, 'import_parent_id IN (?)' => array_values($descendantProducts), 'import_type' => $importExport->getImportType()], 'import_parent_id', 'product_id');

            foreach ($descendantProducts as $i => $importParentId) {
                if (isset($products[$importParentId])) {
                    $importExport->output[self::TABLE_PRODUCT][$i][self::COLUMN_NAME] = $products[$importParentId];
                }
            }
        }

//        Debugger::log($logName . "__invoke end");
    }

}
