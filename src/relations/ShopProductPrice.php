<?php

namespace Wame\ImportExport\Relations;

use App\Utils\Numbers;
use Tracy\Debugger;
use Wame\ImportExport\ImportExport;
use Wame\ImportExport\Relations\Relation;


class ShopProductPrice extends Relation
{
    const TABLE_SHOP_PRODUCT = 'wame_shop_product';
    const TABLE_SHOP_PRODUCT_PRICE = 'wame_shop_product_price';
    const TABLE_SHOP_SETTINGS = 'wame_shop_settings';


    /** @var ImportExport */
    protected $importExport;

    /** @var array */
    protected $shopSettings;

    /** @var array */
    protected $products;

    /** @var array */
    protected $inOutput;

    /** @var integer */
    protected $tax;

    // Price Process Variables
    protected
        $inputI,
        $productXmlPrice,
        $importProductId,
        $shopProductId,
        $productPrice,
        $declineSpecialPrice;

    // Price Table Variables
    protected
        $purchasePrice,
        $purchasePriceWithTax,
        $price,
        $priceWithTax,
        $discount,
        $margin,
        $beforeDiscountPrice,
        $beforeDiscountPriceWithTax;


    function __invoke(ImportExport &$importExport)
    {
        /** Set Import Export Variable */
        $this->importExport = $importExport;

        /** Start Logging */
        $this->relationLog('START');

        /** @var Set InOutput */
        $this->inOutput = $this->getInOutput();

        /** Set Product List */
        $this->products = $this->getProducts();

        /** Set default tax */
        $this->tax = $this->getShopSettings()['defaultTax'];

        foreach ($this->importExport->output[self::TABLE_SHOP_PRODUCT_PRICE] as $i => $item) {

            /** Reset all price variables to null */
            $this->setDefaultVariables($i);

            /** Keep current price if product is descendant */
            if ($this->isDescendant($i) == true) {
                if ($this->setDescendantPrice($i) == true) {
                    $this->processPrice($i);
                    continue;
                }
            }

            /** Set declineSpecialPrice */
            $this->declineSpecialPrice = $this->declineSpecialPrice();

            /** Set Product Price For Output */
            $this->setProductPrice();

            /** Set Product Purchase Price For Output */
            $this->setProductPurchasePrice();

            /** Set Product Discount For Output */
            $this->setDiscount();

            /** Process Price Of One Product In Output */
            $this->processPrice($i);
        }

        $importExport = $this->importExport;

        /** End Logging */
        $this->relationLog('END');

        unset($this->inOutput);
        $this->products = null;
    }

    private function processPrice($i) {
        // If Not Price, then price will be unset from output
        if ($this->price === null || is_array($this->price) || $this->price == 0) {
            $this->unsetFromOutput($i);
            return;
        } else {
            // Else set price to output
            $this->setOutput($i);
        }
    }

    /**
     * Set product price data from xml
     *
     * @param $i
     */
    private function setDefaultVariables($i) {
        // Price Process Variables
        $this->inputI = null;
        $this->productXmlPrice = null;
        $this->importProductId = null;
        $this->shopProductId = null;
        $this->productPrice = null;
        $this->declineSpecialPrice = null;

        // Price Table Variables
        $this->purchasePrice = null;
        $this->purchasePriceWithTax = null;
        $this->price = null;
        $this->priceWithTax = null;
        $this->discount = null;
        $this->margin = null;
        $this->beforeDiscountPrice = null;
        $this->beforeDiscountPriceWithTax = null;


        /** Set inputI */
        $this->inputI = array_search($this->importExport->output[self::TABLE_SHOP_PRODUCT][$i]['import_product_id'] ,array_column($this->importExport->input['produkt'], 'plu'));

        /** Set productXmlPrice */
        $this->productXmlPrice = $this->importExport->input['produkt'][$this->inputI];

        /** Set importProductId */
        $this->importProductId = $this->importExport->output[self::TABLE_SHOP_PRODUCT][$i]['import_product_id'];

        /** Set shopProductId */
        $this->shopProductId = isset($this->products[$this->importProductId]) ? $this->products[$this->importProductId] : null;

        /** Set productPrice from Output */
        $this->productPrice = $this->importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i];
    }


    /**
     * Unset product price from Output
     *
     * @param $i
     */
    private function unsetFromOutput($i) {
        if (!isset($this->importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i])) return;
        unset($this->importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i]);
    }

    /**
     * Set product price if is Descendant
     *
     * @param $i
     * @return bool
     */
    private function setDescendantPrice($i) {
        $price = $this->importExport->getRepository(self::TABLE_SHOP_PRODUCT_PRICE)->findOneBy(['import_type' => $this->importExport->getImportType(), 'shop_product_id' => $this->shopProductId]);
        if ($price) {
            // Price Table Variables
            $this->purchasePrice = $price['purchase_price'];
            $this->purchasePriceWithTax = $price['purchase_price_with_tax'];
            $this->price = $price['price'];
            $this->priceWithTax = $price['price_with_tax'];
            $this->discount = $price['discount'];
            $this->margin = $price['margin'];
            $this->beforeDiscountPrice = $price['before_discount_price'];
            $this->beforeDiscountPriceWithTax = $price['before_discount_price_with_tax'];
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if product price is Descendant
     *
     * @return bool
     */
    private function isDescendant() {
        if (!isset($this->productPrice)) return true;
        if (!isset($this->productPrice['price'])) return true;
        if (is_array($this->productPrice['price'])) return true;
        return false;
    }

    /**
     * Set product price to Output
     *
     * @param $i
     */
    private function setOutput($i) {
        $oldValues = $this->importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i];
        $newValues = [
            'shop_product_id' => $this->shopProductId,
            'import_id' => $this->importProductId.'-'.$this->shopProductId,
            'margin' => $this->margin,
            'tax' => $this->tax,
            'purchase_price' => $this->purchasePrice,
            'purchase_price_with_tax' => $this->purchasePriceWithTax,
            'price' => $this->price,
            'price_with_tax' => $this->priceWithTax,
            'before_discount_price' => $this->beforeDiscountPrice,
            'before_discount_price_with_tax' => $this->beforeDiscountPriceWithTax,
            'discount' => $this->discount,
            'create_date' => date('Y-m-d H:i:s')
        ];

        $this->importExport->output[self::TABLE_SHOP_PRODUCT_PRICE][$i] = array_replace($oldValues, $oldValues, $newValues);
    }

    /**
     * Prepare Product Discount For Output
     */
    private function setDiscount() {
        if ($this->declineSpecialPrice) {
            $this->discount = (is_numeric(floatval($this->beforeDiscountPriceWithTax)) && is_numeric(floatval($this->priceWithTax))) ? ($this->beforeDiscountPriceWithTax-$this->priceWithTax) : 0;
        } else {
            $this->discount = 0;
        }
    }

    /**
     * Prepare Purchase Product Price For Output
     */
    private function setProductPurchasePrice() {
        // If Is Set Purchase Price
        if (isset($this->productPrice['purchase_price'])) {
            // Set Purchase Price And Margin
            $this->purchasePrice = is_numeric(floatval($this->productPrice['purchase_price'])) ? floatval($this->productPrice['purchase_price']) : 0;
            $this->purchasePriceWithTax = is_numeric(floatval($this->productPrice['purchase_price'])) ? floatval($this->productPrice['purchase_price']) + (($this->productPrice['purchase_price'] / 100) * $this->tax) : 0;

            $productMargin =  $this->productPrice['price_with_tax'] / 100;
            $this->margin = ($this->productPrice['purchase_price'] / $productMargin);

        } else {
            // Set Default Purchase Price And Default Margin
            $this->purchasePrice = 0;
            $this->purchasePriceWithTax = 0;
            $this->margin = isset($this->getShopSettings()[$this->getMarginName()]) ? $this->getShopSettings()[$this->getMarginName()] : 0;
        }
    }

    /**
     * Prepare Product Price For Output
     */
    private function setProductPrice() {
        // If Declined Product Price
        if ($this->declineSpecialPrice)
        {
            // Set Before Discount Price
            $this->beforeDiscountPrice = is_numeric(floatval($this->productPrice['price'])) ? floatval($this->productPrice['price']) : 0;
            $this->beforeDiscountPriceWithTax = is_numeric(floatval($this->productPrice['price_with_tax'])) ? floatval($this->productPrice['price_with_tax']) : 0;

            // Set FIXED Price From XML
            $this->price = is_numeric(floatval($this->productXmlPrice['predajneceny']['bezdph5'])) ? floatval($this->productXmlPrice['predajneceny']['bezdph5']) : 0;
            $this->priceWithTax = is_numeric(floatval($this->productXmlPrice['predajneceny']['sdph5'])) ? floatval($this->productXmlPrice['predajneceny']['sdph5']) : 0;
        }
        // If Not Declined Product Price
        else
            {
            // Reset Before Discount Price
            $this->beforeDiscountPrice = 0;
            $this->beforeDiscountPriceWithTax = 0;

            // Set Price From Output
            $this->price = is_numeric(floatval($this->productPrice['price'])) ? floatval($this->productPrice['price']) : 0;
            $this->priceWithTax = is_numeric(floatval($this->productPrice['price_with_tax'])) ? floatval($this->productPrice['price_with_tax']) : 0;
        }
    }

    /**
     * Get Product Decline Special Price Value
     *
     * @return int
     */
    private function declineSpecialPrice() {
        if (isset($this->productXmlPrice['predajneceny']['bezdph5']) && isset($this->productXmlPrice['predajneceny']['sdph5'])) {
           if ($this->productXmlPrice['predajneceny']['bezdph5'] > 0 && $this->productXmlPrice['predajneceny']['sdph5'] > 0) return 1;
        }
        return 0;
    }

    /**
     * Get Prices From Chunk
     *
     * @return mixed
     */
    private function getPrices() {
        return $this->importExport->getRepository(self::TABLE_SHOP_PRODUCT_PRICE)->getTable()
            ->select('id, shop_product_id, purchase_price, margin, prices_group, price, price_with_tax, discount, discount_type')
            ->where(['status' => 1, 'import_type' => $this->importExport->getImportType()])->fetchPairs('shop_product_id');
    }

    /**
     * Get Product From Chunk
     *
     * @return mixed
     */
    private function getProducts() {
        return $this->importExport->getRepository(self::TABLE_SHOP_PRODUCT)->getPairs(['import_type' => $this->importExport->getImportType(), 'import_product_id IN (?)' => $this->inOutput], 'import_product_id', 'product_id');
    }

    /**
     * Get Output
     *
     * @param $inOutput
     * @return mixed
     */
    private function getInOutput() {
        return array_column($this->importExport->output[self::TABLE_SHOP_PRODUCT], 'import_product_id');
    }

    /**
     * Return Shop Settings
     *
     * @return mixed
     */
    private function getShopSettings() {
        return $this->importExport->getRepository(self::TABLE_SHOP_SETTINGS)->getPairs(['name IN (?)' => [$this->getMarginName(), 'defaultTax'], 'lang' => 'sk'], 'name', 'value');
    }

    /**
     * Return Margin Name
     *
     * @return string
     */
    private function getMarginName() {
        return $this->importExport->getImportType() . 'Margin';
    }

    /**
     * Relation message
     *
     * @param null $message
     */
    private function relationLog($message = null) {
        $logName = $this->importExport->getImportType() . " | ShopProductPrice relation | " . ($message ? '--' . $message . '--' : '');
        echo $logName . "\n";
    }
}
