<?php

namespace Wame\ImportExport\Converters;

interface Converter
{
    public function __invoke(array $input): array;

}