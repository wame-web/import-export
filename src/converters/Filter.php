<?php

namespace Wame\ImportExport\Converters;

/**
 * Class Filter
 * @package Wame\ImportExport\Converters
 * @deprecated
 */
class Filter implements Converter
{
    /** @var string */
    protected $key;

    /** @var mixed */
    protected $value;


    function __construct(string $key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }


    public function __invoke(array $input): array
    {
        if($input[$this->key] == $this->value) {
            return null;
        }

        return $input;
    }

}