<?php

namespace Wame\ImportExport\Converters;

class Negate implements Converter
{
    /** @var string */
    protected $key;


    function __construct(string $key)
    {
        $this->key = $key;
    }


    public function __invoke(array $input): array
    {
        $input[$this->key] = !$input[$this->key];
        return $input;
    }

}