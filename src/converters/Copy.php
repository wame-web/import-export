<?php

namespace Wame\ImportExport\Converters;


class Copy implements Converter
{
    /** @var string */
    protected $from;

    /** @var string */
    protected $to;


    function __construct(string $to, string $from)
    {
        $this->to = $to;
        $this->from = $from;
    }


    public function __invoke(array $input): array
    {
        $input[$this->to] = $input[$this->from];

        return $input;
    }

}
