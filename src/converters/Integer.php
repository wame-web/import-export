<?php

namespace Wame\ImportExport\Converters;


class Integer implements Converter
{
    /** @var string */
    protected $column;


    function __construct(string $column)
    {
        $this->column = $column;
    }


    public function __invoke(array $input): array
    {
        $input[$this->column] = (int) $input[$this->column];

        return $input;
    }

}
