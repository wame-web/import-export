<?php

namespace Wame\ImportExport\Converters;

use Nette\Utils\Random;
use Nette\Utils\Strings;


class Webalize implements Converter
{
    /** @var string */
    protected $from;

    /** @var string */
    protected $to;

    /** @var string */
    protected $charlist;


    function __construct(string $to, string $from = null, string $charlist = null)
    {
        $this->to = $to;
        $this->from = $from ?? $to;
        $this->charlist = $charlist;
    }


    public function __invoke(array $input): array
    {
    	if (empty($input[$this->from]) || $input[$this->from] == '') {
			$input[$this->from] = 'unknown';
		}

		$alias = Strings::webalize($input[$this->from], $this->charlist);

        $input[$this->to] = $alias == '' ? Random::generate(10) : $alias;

        return $input;
    }

}
