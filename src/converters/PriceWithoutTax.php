<?php

namespace Wame\ImportExport\Converters;

class PriceWithoutTax implements Converter
{
    /** @var string */
    protected $key;

    /** @var string */
    protected $priceKey;

    /** @var string */
    protected $taxKey;


    function __construct(string $key = 'price', string $priceKey = 'price_with_tax', string $taxKey = 'tax')
    {
        $this->key = $key;
        $this->priceKey = $priceKey;
        $this->taxKey = $taxKey;
    }

    public function __invoke(array $input): array
    {
        $input[$this->key] = $this->getPriceWithoutTax($input[$this->priceKey], $input[$this->taxKey], 2);
        return $input;
    }


    /**
     * Get price without tax
     *
     * @param float $priceWithTax price with tax
     * @param float $tax tax
     * @param bool|int $round round precision
     * @return float
     */
    private function getPriceWithoutTax(float $priceWithTax, float $tax, $round = false)
    {
        $priceWithoutTax = $tax ? $priceWithTax / (1 + ($tax / 100)) : $priceWithTax;

        return $round === false ? $priceWithoutTax : round($priceWithoutTax, $round);
    }

}