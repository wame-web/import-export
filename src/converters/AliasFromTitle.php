<?php

namespace Wame\ImportExport\Converters;

use Nette\Utils\Strings;

class AliasFromTitle
{
    public function __invoke(array $input)
    {
        $input['alias'] = Strings::webalize($input['title']  . '-' . substr(uniqid(), '-4'));
        return $input;
    }

}