<?php

namespace App\Model\ImportExport\Converters;

use Nette\Utils\Strings;

class NormalizeFileName
{
    /** @var string */
    protected $from;

    /** @var string */
    protected $to;


    function __construct(string $to, string $from = null)
    {
        $this->to = $to;
        $this->from = $from ?? $to;
    }


    public function __invoke(array $input): array
    {
        $filename = Strings::webalize($input[$this->from], '._');
        
        $explode = explode('.', $filename);
        $end = end($explode);
        $extensions = ['gif', 'png', 'jpg'];
        
        if(!in_array($end, $extensions)) {
            $filename .= '.png';
        }

        $input[$this->to] = $filename;
        return $input;
    }

}