<?php

namespace Wame\ImportExport\Converters;

class MoneyStatus
{
    public function __invoke(array $input)
    {
        $input['status'] = $input['hidden'] ? 2 : ($input['deleted'] ? 0 : 1);
        return $input;
    }

}