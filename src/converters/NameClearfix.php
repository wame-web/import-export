<?php

namespace Wame\ImportExport\Converters;

use Nette\Utils\Strings;

class NameClearfix implements Converter
{
    public function __invoke(array $input): array
    {
        $input['name'] = str_replace('-', '', Strings::webalize($input['name']));
        return $input;
    }

}