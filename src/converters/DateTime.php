<?php

namespace Wame\ImportExport\Converters;


class DateTime implements Converter
{
    /** @var string */
    private $column;


    public function __construct($column)
    {
        $this->column = $column;
    }


    public function __invoke(array $input): array
    {
        $input[$this->column] = date('Y-m-d H:i:s', strtotime($input[$this->column]));

        return $input;
    }

}
