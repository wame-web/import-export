<?php

namespace Wame\ImportExport\Converters;

class Remove implements Converter
{
    /** @var string */
    protected $key;


    function __construct(string $key)
    {
        $this->key = $key;
    }


    public function __invoke(array $input): array
    {
        unset($input[$this->key]);
        return $input;
    }

}