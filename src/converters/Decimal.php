<?php

namespace Wame\ImportExport\Converters;


class Decimal implements Converter
{
    /** @var string */
    protected $column;


    function __construct(string $column)
    {
        $this->column = $column;
    }


    public function __invoke(array $input): array
    {
        $input[$this->column] = str_replace(',', '.', $input[$this->column]);

        return $input;
    }

}
