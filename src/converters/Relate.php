<?php

namespace Wame\ImportExport\Converters;

use Wame\ImportExport\BaseRepository;

class Relate implements Converter
{
    /** @var BaseRepository */
    protected $repository;

    /** @var string */
    protected $toId;

    /** @var string */
    protected $toImportId;

    /** @var string */
    protected $fromId;

    /** @var array */
    private $items;


    function __construct(BaseRepository $repository, string $toId, string $toImportId, string $fromId)
    {
        $this->repository = $repository;
        $this->toId = $toId;
        $this->toImportId = $toImportId;
        $this->fromId = $fromId;
    }


    public function __invoke(array $input): array
    {
        $items = $this->getItems();
        $input[$this->toId] = $items[$input[$this->fromId]] ?? null;

        return $input;
    }


    private function getItems()
    {
        if(!$this->items) {
            $this->items = $this->repository->getPairs([], $this->toImportId, 'id');
        }

        return $this->items;
    }

}