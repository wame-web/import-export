<?php

namespace Wame\ImportExport\Converters;

class Date
{
    /** @var string */
    protected $to;

    /** @var string */
    protected $from;

    /** @var string */
    protected $modify;


    function __construct(string $to, string $from, string $modify)
    {
        $this->to = $to;
        $this->from = $from;
        $this->modify = $modify;
    }


    public function __invoke(array $input): array
    {
        $input[$this->to] = (new \DateTime($input[$this->from]))->modify($this->modify);

        return $input;
    }

}
