<?php

namespace Wame\ImportExport\Converters;

class PriceWithTax implements Converter
{
    /** @var string */
    protected $key;

    /** @var string */
    protected $priceKey;

    /** @var string */
    protected $taxKey;


    function __construct(string $key = 'price_with_tax', string $priceKey = 'price', string $taxKey = 'tax')
    {
        $this->key = $key;
        $this->priceKey = $priceKey;
        $this->taxKey = $taxKey;
    }


    public function __invoke(array $input): array
    {
        $input[$this->key] = $this->getPriceWithTax($input[$this->priceKey], $input[$this->taxKey], 2);
        return $input;
    }


    /**
     * Get price with tax
     *
     * @param float $priceWithoutTax price without tax
     * @param float $tax tax
     * @param bool|int $round round precision
     * @return float
     */
    private function getPriceWithTax(float $priceWithoutTax, float $tax, $round = false)
    {
        $priceWithTax = $tax ? $priceWithoutTax * (($tax / 100) + 1) : $priceWithoutTax;

        return $round === false ? $priceWithTax : round($priceWithTax, $round);
    }

}