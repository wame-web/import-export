<?php

namespace Wame\ImportExport\Converters;

class Condition implements Converter
{
    /** @var string */
    private $to;

    /** @var string */
    private $from;

    /** @var array */
    private $data = [];


    function __construct(string $to, string $from, array $data)
    {
        $this->to = $to;
        $this->from = $from;
        $this->data = $data;
    }


    public function __invoke(array $input): array
    {
        if (array_key_exists($input[$this->from], $this->data)) {
            $input[$this->to] = $this->data[$input[$this->from]];
        }

        return $input;
    }

}