<?php

namespace Wame\ImportExport;


class Export extends ImportExport
{
    /**
     * Add source
     *
     * @param string $name name
     * @param BaseRepository $repository repository
     * @param array $criteria criteria
     * @param string|null $indexBy index by
     * @return $this
     */
    public function addSource(string $name, \App\Model\BaseRepository $repository, array $criteria = [], string $indexBy = null)
    {
        if (!$this->settings) {
            $this->settings = $this->class->settings;
        }

        foreach ($criteria as $key => &$val) {
            if ($val == "LAST_EXPORT_DATE") {
                $val = $this->settings['date'];
            }

            if (is_string($val) && substr($val, 0, 1) == '&') {
                $val = $this->prepareColumnFromTable($val);
            }
        }

        $items = $repository->findBy($criteria)->fetchAll();
        $indexedItems = [];

        if (!$indexBy) {
            foreach ($items as $item) {
                $indexedItems[] = $item;
            }
        } else {
            foreach ($items as $item) {
                $indexedItems[$item[$indexBy]] = $item;
            }
        }

        $this->data[$name] = $indexedItems;

        return $this;
    }


    /** {@inheritdoc} */
    public function setInput(array $input = []): ImportExport
    {
        $this->input[] = $input;

        return $this;
    }


    /**
     * Prepare column from table
     * single or multiple IN
     *
     * @param string $val
     *
     * @return array
     */
    protected function prepareColumnFromTable($val)
    {
        preg_match("/&(\w+)(.|:)(\w*.*)/", $val, $match);

        list ($full, $table, $operator, $column) = $match;

        if ($operator == ':') {
            $return = [];

            foreach ($this->data[$table] as $row) {
                $return[] = $row[$column];
            }

            return $return;
        } elseif ($operator == '.') {
            return $this->data[$table][$column];
        }

        return $val;
    }

}