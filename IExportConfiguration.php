<?php

namespace Wame\ImportExport;

use Nette\Utils\Strings;

abstract class IExportConfiguration implements IDataConfiguration
{

    public abstract function export();

    /**
     * @return Form|null
     */
    public function getSettingsEditor()
    {

    }

    /**
     * @return string
     */
    public function getIdentifierName()
    {
        return 'export' . Strings::capitalize(str_replace('-', '', Strings::webalize($this->getName())));
    }

    /**
     * @return string
     */
    public function getSettingsEditorName()
    {
        return $this->getIdentifierName() . 'Settings';
    }

    public function isImport()
    {
        return false;
    }

    public function isExport()
    {
        return true;
    }

}